//
//  ContractReportHinders.swift
//  Cities
//
//  Created by User on 01/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ContractReportHinders: NSObject {

    var iContractReportId : Int = 0
    var iHinderType : Int = 0
    
    override init(){}
    
    init(/*_iContractReportId : Int ,*/_iHinderType : Int )
    {
//        iContractReportId = _iContractReportId
        iHinderType =  _iHinderType
    }
    
    
    func getContractReportHindersFromDic(dic : Dictionary<String, AnyObject>) -> ContractReportHinders
    {
        let c = ContractReportHinders()
        
        if dic["iHinderType"] != nil
        {
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iHinderType"]!) as? NSNumber {
                c.iHinderType = item as! Int}
        }
        return c
    }
    
    
    
    func getDicFromContractReportHinders() -> Dictionary<String, AnyObject>
    {
        var dic = Dictionary<String, AnyObject>()
        
//        dic["iContractReportId"] = iContractReportId as AnyObject
        dic["iHinderType"] = iHinderType as AnyObject
        
      return dic
    }
    
}

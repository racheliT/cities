//
//  ContractReportFiles.swift
//  Cities
//
//  Created by User on 01/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ContractReportFiles: NSObject {

    var iContractReportId : Int = 0
    var nvFileName : String = ""
    var nvFilePath : String = ""
    var iFileType : Int = 0
    var nvLat : String = "" /*- נקודת רוחב*/
    var nvLong : String = "" //- נקודת אורך
    
    
    override init(){}
    init(_iContractReportId : Int, _nvFileName : String, _nvFilePath : String, _iFileType : Int, _nvLat : String, _nvLong : String)
    {
        iContractReportId = _iContractReportId
        nvFileName = _nvFileName
        nvFilePath = _nvFilePath
        iFileType = _iFileType
        nvLat = _nvLat
        nvLong = _nvLong
    }

    
    func getDicFromContractReportFiles() -> Dictionary<String, AnyObject>
    {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iContractReportId"] = iContractReportId as AnyObject
        dic["nvFileName"] = nvFileName as AnyObject
        dic["nvFilePath"] = nvFilePath as AnyObject
        dic["iFileType"] = iFileType as AnyObject
        dic["nvLat"] = nvLat as AnyObject
        dic["nvLong"] = nvLong as AnyObject
        
        return dic
    }
}

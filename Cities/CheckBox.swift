//
//  CheckBox.swift
//  Cities
//
//  Created by User on 31/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class CheckBox: UIButton {

    
    var isChecked:Bool = false{
        didSet{
            if isChecked == true
            {
//                self.setTitle("\u{f046}", for: .normal)
                self.setImage(UIImage(named: "checkboxCircle"), for: .normal)
            }
            else
            {
//                self.setTitle("\u{f096}", for: .normal)
                self.setImage(UIImage(named: "circle"), for: .normal)
            }
        }
    }
    override func awakeFromNib() {
        
        self.isChecked = false
        self.setImage(UIImage(named: "circle"), for: .normal)
        
//        self.addTarget(self, action: #selector(updateBtn), for: UIControlEvents.touchUpInside)
    }
    
//    func updateBtn() {
//        isChecked = !isChecked
//    }

}

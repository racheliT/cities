//
//  SectionReportTableViewCell.swift
//  Cities
//
//  Created by User on 18/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class SectionReportTableViewCell: UITableViewCell {
    
    //MARK: - views
    @IBOutlet weak var imgSection: UIImageView!
    @IBOutlet weak var openOrCloseSection: UIButton!
    @IBOutlet weak var lblTitleSection: UILabel!
    //MARK: - actions
    
    //MARK: - variables
    var numSection : Int = 0
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ContractViewController.swift
//  Cities
//
//  Created by User on 07/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ContractViewController: UIViewController {
    
    //MARK: - views
    @IBOutlet weak var tblContract: UITableView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var imgDetail: UIImageView!
    
    @IBOutlet weak var lblProjectName: UILabel!
    
    //MARK: - variables
    var projectId : Int = 0
    var tempArr1 : Array<Project> = Array<Project>()
    var projectNameArr : Array<Any> = []
    var tempArr2 : Array<Contract> = []
    var contractNumberArr :Array<Any> = []
    var contractorNameArr : Array<String> = []
    var contractIdArr : Array<Any> = []
    var projectName : String = ""
    
    
    //MARK: - actions
    @IBAction func btnBackAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListProjectsViewController") as! ListProjectsViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnExitAction(_ sender: Any) {
        exit(0)
    }
    
    override func viewDidLoad() {
       
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportsViewController") as! ReportsViewController
        tblContract.dataSource = self
        tblContract.delegate = self
        
        lblProjectName.text = CommonVars.sharedInstance.projectName
//        btnBack.setImage(UIImage.init(named : "back"), for: .normal)
//        imgDetail.image = UIImage(named: "detail@2x")
        getContractsByProject()
        

        //                    self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - function
    func getDetailsOfProject(projectid : Int/*, projectname : String*/){
        projectId = projectid
        /*projectName = projectname*/
    }
    
    
    func getContractsByProject()
    {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iProjectId"] = /*projectId*/CommonVars.sharedInstance.projectId as AnyObject
        //        let person = Person(userName: "ווביט פיתוח", passWord: "147852",personType: 1)
        //        dic["person"] = person.getDicFromPerson() as AnyObject
        //        dic["date"] = khjkh
        //        print(dic)
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            //            print(response!)
            if let result = response as? Array<Dictionary<String, AnyObject>> {
                //       -         print(result)
                let c = Contract()
                
                for item in result
                {
                    self.tempArr2.append(c.getContractFromDic(dic: item))
                    
                }
                for item in self.tempArr2
                {
                    self.contractNumberArr.append(item.nvContractNumber)
                    self.contractorNameArr.append(item.nvContractorName)
                    self.contractIdArr.append(item.iContractId)

                }
                 self.tblContract.reloadData()
            }
           
        }, failure: {
            (dataTask, error) -> Void in
            
//            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetContractsToProject)
    }
}


extension ContractViewController : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contractNumberArr.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblContract.dequeueReusableCell(withIdentifier: "ContractTableViewCell") as! ContractTableViewCell

        //design
//        cell.layer.borderWidth = 1
//        cell.backgroundColor = UIColor.mrhFrenchBlueWithLowOpacity

        cell.layer.borderColor = UIColor.white.cgColor
        /////
        cell.lblTiTle.text = "\(contractorNameArr[indexPath.row] as? String)  \(contractNumberArr[indexPath.row])"
//        cell.lblContractName.text = contractorNameArr[indexPath.row]

        return cell
    }
}

extension ContractViewController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tblContract.dequeueReusableCell(withIdentifier: "ContractTableViewCell")as! ContractTableViewCell
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportsViewController") as! ReportsViewController
        cell.contentView.backgroundColor = UIColor.red
        
        
        CommonVars.sharedInstance.contractId = contractIdArr[indexPath.row] as! Int
     
      
        
//         vc.getContractNameAndNum(name: contractorNameArr[indexPath.row])
//        vc.getDetailsToContract(contName: contractorNameArr[indexPath.row], contNum: contractNumberArr[indexPath.row] as! Int)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

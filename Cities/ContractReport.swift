//
//  Contract.swift
//  Cities
//
//  Created by User on 31/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ContractReport: NSObject {
    
    var iContractReportId : Int = 0
    var iContractId : Int = 0
    var iProjectId : Int = 0
    var iPersonSupervisionId : Int = 0
    var iNumToolsInArea : Int = 0
    var iNumWorkersInArea : Int = 0
    var bIsWorkContractSigned : Bool = true
    var bIsExecutionEngineerExists : Bool = true
    var bIsForemanExists : Bool = true
    var bInSite : Bool = true
    var nvComment : String = ""
    var iWorkQualityRating : Int = 0
    var iProgressType : Int = 0
    var lContractReportHinders : Array<ContractReportHinders> = []
    var lContractReportProfessionals : Array<ContractReportProfessionals> = []
    var lContractReportWorkProgressFiles : Array<ContractReportFiles> = []
    var lContractReportNuisancesFiles : Array<ContractReportFiles> = []
    var lContractReportComponents : Array<ContractReportComponents> = []
    
    override init(){
    }
    
    
    init(_iContractId : Int,_iProjectId : Int, _iPersonSupervisionId : Int, _iNumToolsInArea : Int, _iNumWorkersInArea : Int, _bIsWorkContractSigned : Bool, _bIsExecutionEngineerExists : Bool, _bIsForemanExists : Bool, _bInSite : Bool,_nvComment : String, _iWorkQualityRating : Int, _iProgressType : Int,
         _lContractReportHinders : Array<ContractReportHinders>, _lContractReportProfessionals:
        Array<ContractReportProfessionals>,_lContractReportWorkProgressFiles : Array<ContractReportFiles>,
         _lContractReportNuisancesFiles : Array<ContractReportFiles>, _lContractReportComponents:
        Array<ContractReportComponents>)
    {
        iContractId = _iContractId
        iProjectId = _iProjectId
        iPersonSupervisionId = _iPersonSupervisionId
        iNumToolsInArea = _iNumToolsInArea
        iNumWorkersInArea = _iNumWorkersInArea
        bIsWorkContractSigned = _bIsWorkContractSigned
        bIsExecutionEngineerExists = _bIsExecutionEngineerExists
        bIsForemanExists = _bIsForemanExists
        bInSite = _bInSite
        nvComment = _nvComment
        iWorkQualityRating = _iWorkQualityRating
        iProgressType = _iProgressType
        lContractReportHinders = _lContractReportHinders
        lContractReportProfessionals = _lContractReportProfessionals
        lContractReportWorkProgressFiles = _lContractReportWorkProgressFiles
        lContractReportNuisancesFiles =  _lContractReportNuisancesFiles
        lContractReportComponents = _lContractReportComponents
    
    }
    
    func getDicFromReport() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
   
        dic["iProjectId"] = iProjectId as AnyObject
        dic["iContractId"] = iContractId as AnyObject
        dic["iPersonSupervisionId"] = iPersonSupervisionId as AnyObject
        dic["iNumToolsInArea"] = iNumToolsInArea as AnyObject
        dic["iNumWorkersInArea"] = iNumWorkersInArea as AnyObject
        dic["bIsWorkContractSigned"] = bIsWorkContractSigned as AnyObject
        dic["bIsExecutionEngineerExists"] = bIsExecutionEngineerExists as AnyObject
        dic["bIsForemanExists"] = bIsForemanExists as AnyObject
        dic["bInSite"] = bInSite as AnyObject
        dic["nvComment"] = nvComment as AnyObject
        dic["iWorkQualityRating"] = iWorkQualityRating as AnyObject
        dic["iProgressType"] = iProgressType as AnyObject
        
        
        
        var dicArr1 = Array<Dictionary<String, AnyObject>>()
        for item in lContractReportHinders {
            dicArr1.append(item.getDicFromContractReportHinders())
        }
        dic["lContractReportHinders"] = dicArr1 as AnyObject
        
        //////////////////
        var dicArr2 = Array<Dictionary<String, AnyObject>>()
        for item in lContractReportProfessionals {
            dicArr2.append(item.getDicFromContractReportProfessionals())
        }
        dic["lContractReportProfessionals"] = dicArr2 as AnyObject
        
        //////////////////////
        var dicArr3 = Array<Dictionary<String, AnyObject>>()
        for item in lContractReportWorkProgressFiles {
            dicArr3.append(item.getDicFromContractReportFiles())
        }
        dic["lContractReportWorkProgressFiles"] = dicArr3 as AnyObject
        ////////////////
        
        var dicArr4 = Array<Dictionary<String, AnyObject>>()
        for item in lContractReportNuisancesFiles {
            dicArr4.append(item.getDicFromContractReportFiles())
        }
        dic["lContractReportNuisancesFiles"] = dicArr4 as AnyObject
        
        //////////////
        var dicArr5 = Array<Dictionary<String, AnyObject>>()
        for item in lContractReportComponents {
            dicArr5.append(item.getDicFromContractReportComponents())
        }
        dic["lContractReportComponents"] = dicArr5 as AnyObject
//        print(dic)
        return dic
    }
    
    
    
    func getReportFromDic(dic :Dictionary<String, AnyObject>) -> ContractReport {
        
        let contract = ContractReport()
        
        if dic["iContractId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iNumToolsInArea"]!) as? NSNumber {
                contract.iContractId = item as! Int
                
            }
        }
        return contract
    }

    
}

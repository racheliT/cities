//
//  WeeklyReportingTableViewCell.swift
//  Cities
//
//  Created by User on 18/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class WeeklyReportingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var segment1: UISegmentedControl!
    @IBOutlet weak var imgErrorDropDown: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!

    @IBOutlet weak var imgStar1: UIImageView!
    //MARK: - views
   
    @IBOutlet weak var tblDropDown1: UITableView!
    @IBOutlet weak var lblSatisfaction: UILabel!
    @IBOutlet weak var btnDropDown1: UIButton!
    @IBOutlet weak var btnAlert: UIButton!
    @IBOutlet weak var lblTopToTblConstraint: NSLayoutConstraint!
    
    
    //MARK: - variables
    var dropDownNamesArr : Array<Any> = []
    var professionalsArr : Array<TablesToContract> = Array<TablesToContract>()
    var selectionsRowArr : Array<ContractReportProfessionals> = []
    var isRowSelected : Array<Bool> = []
    var showImageIndex = -1
    var hideImageIndex = -1
    var countRowSelected = 0
    var isSelectedStarArr : Array<Bool> = [true,true,true,true,true]
    var segmentIndex = 0
    var countStars = 0
    var dropDownSelectionsIdArr : Array<Any> = []
    var dropDownIdArr : Array<Any> = []
    var flagIsEdit = false

    
    //MARK: - actions
    func fillDropDown1(arr: Array<ContractReportProfessionals>)
    {
        for i in arr
        {
            dropDownSelectionsIdArr.append(i.iContractProfessionalType)
        }
    }
    
    
    func getFieldsToEdit()
    {
        var dic = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = 1 as AnyObject
        dic["iContractReportId"] = 31 as AnyObject
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            if let result = response as? Dictionary<String, AnyObject>{
                
                var get = GetContractReport()
                get = get.GetContractReportFromDic(dic: result)
              
                self.fillDropDown1(arr: get.lContractReportProfessionals)
                self.tblDropDown1.reloadData()
//                self.fillStars(get.iWorkQualityRatin)
//                self.fillsegment(get.iProgressType)
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetContractReport)
    }
    
    
    
    
    @IBAction func btnStarAction(_ sender: UIButton) {
        //כוכב אחד תמיד צריך להיות לחוץ
        // כך  ש countStars תמיד צריך להיות מאותחל באחד
        countStars += 1
        if isSelectedStarArr[sender.tag] == true
        {
            countStars += 1
            sender.setImage(UIImage(named: "ratingFull"), for: .normal)
            isSelectedStarArr[sender.tag] = false
        }
        else
        {
            countStars -= 2
            sender.setImage(UIImage(named: "icon1"), for: .normal)
//            sender.imageView?.isHidden = true
                 isSelectedStarArr[sender.tag] = true
            
        }
    }
    
    @IBAction func segmaent1Action(_ sender: UISegmentedControl) {
        switch segment1.selectedSegmentIndex {
        case 0:
            segmentIndex = 555
        case 1:
            segmentIndex = 554
        case 2:
            segmentIndex = 553
        default:
            segmentIndex = 554
        }
    }
    
    @IBAction func btnDropDown1Action(_ sender: UIButton) {
        
        if tblDropDown1.isHidden == true
        {tblDropDown1.isHidden = false}
        
        else
        {tblDropDown1.isHidden = true
            btnDropDown1.setTitle("בחרת \(countRowSelected) רשומות", for: .normal)
        }
        tblDropDown1.reloadData()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        segment1.selectedSegmentIndex = 1
        imgErrorDropDown.isHidden = true
        self.contentView.bringSubview(toFront: tblDropDown1)
        
        lblTopToTblConstraint.constant = 23
        btnDropDown1.layer.cornerRadius = 7
        tblDropDown1.layer.cornerRadius = 7
        tblDropDown1.isHidden = true
        tblDropDown1.delegate = self
        tblDropDown1.dataSource = self
        getListProfessionals()
        let alert = UIAlertController(title: "title", message: "rfefrf", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok", style: .default))
   
 
 
 
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - fnctions
  
    
    func getListProfessionals()
    {
        var dic = Dictionary<String,AnyObject>()
        
        dic["iContractId"] = CommonVars.sharedInstance.contractId as AnyObject
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            print("response : \(String(describing: response))")
            
            if let result = response as? Array<Dictionary<String, AnyObject>> {
                let t = TablesToContract()
                //                print(t.nvName)
                print(result)
                //  project = project.getPersonFromDic(dic: result)
                
                //
                for item in result
                {
                    if let key = item["Key"] as? String, key == "Professionals" {
                        if let res1 = item["Value"] as? Array<Dictionary<String, AnyObject>> {
                            for item2 in res1 {
                                self.professionalsArr.append(t.getTablesToContractFromDic(dic: item2))
                            }
                        }
                    }
                    //                    self.barriersArr.append(tbl.getTableFromDic(dic: item))
                }
                //add to arr the name of the barriers
                for  i in self.professionalsArr
                {
                    self.dropDownNamesArr.append(i.nvName)
                    self.dropDownIdArr.append(i.iId)
                }
                self.tblDropDown1.reloadData()
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print("error")
            
        }, funcName: api.sharedInstance.GetCodeTablesToContract)
  
    }

    func validationCheck() -> Bool
    {
        var flagIsFieldFull = false
 
        var countFullFields = 0
        if countRowSelected == 0//check dropDown
        {
            imgErrorDropDown.isHidden = false
        }
        else
        {
           imgErrorDropDown.isHidden = true
            countFullFields += 1
        }
        
        if countFullFields == 1
        {
            flagIsFieldFull = true
        }
        else
        {
            flagIsFieldFull = false
        }
      
        return flagIsFieldFull
    }
    
    
    
}

extension WeeklyReportingTableViewCell : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        for i in dropDownNamesArr
        {
        isRowSelected.append(false)
        }
        return dropDownNamesArr.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableViewCell", for: indexPath) as! DropDownTableViewCell
        cell.layer.borderWidth = 1
        cell.lblDropDown.text = dropDownNamesArr[indexPath.row] as? String
        
        if flagIsEdit == true
        {
            if dropDownSelectionsIdArr[indexPath.row] as! Int == dropDownIdArr[indexPath.row] as! Int
            {
                cell.imgIndexDropDown.image = UIImage(named: "checkboxCircle")
            }
        }
            
        
//        if showImageIndex == indexPath.row
//        {
        if isRowSelected[indexPath.row] == true
        {
            cell.imgIndexDropDown.image = UIImage(named: "checkboxCircle")
            showImageIndex = -1
        }
        
//        if hideImageIndex == indexPath.row
//        {
        if isRowSelected[indexPath.row] == false
        {
            cell.imgIndexDropDown.image = UIImage(named: "circle")
            hideImageIndex = -1
        }
       
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }
}

extension WeeklyReportingTableViewCell : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
 
        let c = ContractReportProfessionals()
        var indexSelected : Int = 0
        
        indexSelected = professionalsArr[indexPath.row].iId
        c.iContractProfessionalType = indexSelected
        selectionsRowArr.append(c)
        
        
        if isRowSelected[indexPath.row] == true
        {
            showImageIndex = indexPath.row
            isRowSelected[indexPath.row] = false
            countRowSelected += 1
        }
        else if isRowSelected[indexPath.row] == false
        {
            hideImageIndex = indexPath.row
            isRowSelected[indexPath.row] = true
            countRowSelected -= 1
        }
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
       
        
       
        
    }
}

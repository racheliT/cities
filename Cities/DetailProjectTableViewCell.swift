//
//  DetailProjectTableViewCell.swift
//  Cities
//
//  Created by User on 18/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class DetailProjectTableViewCell: UITableViewCell {
    
    //MARK: - view
    @IBOutlet weak var btnClickDropDown: UIButton!
    @IBOutlet weak var heightNamecontractor: NSLayoutConstraint!
    @IBOutlet weak var tblDropContract: UITableView!

    
     //MARK: - variables
    var arr = ["a","b","c"]
    
    
    //MARK: - actions
    @IBAction func btnClickDropNumContractAction(_ sender: Any) {
        
        if tblDropContract.isHidden == true
        {tblDropContract.isHidden = false}
            
        else
         {tblDropContract.isHidden = true}
        //tblDropContract.reloadData()
    }


    override func awakeFromNib() {
        super.awakeFromNib()
       
        
        tblDropContract.isHidden = true
         heightNamecontractor.constant = 0.0
        // Initialization code
        
        tblDropContract.dataSource = self
        tblDropContract.delegate = self  
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
}


extension DetailProjectTableViewCell: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // let cell = tableView.cellForRow(at: indexPath)
        btnClickDropDown.setTitle(arr[indexPath.row], for: .normal)
        self.tblDropContract.isHidden = true
        self.tblDropContract.reloadData()
    }
}


extension DetailProjectTableViewCell : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableViewCell", for: indexPath) as! DropDownTableViewCell
        cell.lblDropDown.text = arr[indexPath.row]
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return arr.count
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
    return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.frame.height / 15
    }
    
}

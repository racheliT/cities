//
//  CommonVars.swift
//  Cities
//
//  Created by User on 23/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class CommonVars: NSObject {
    
    override init() {
        
    }
    
    static let sharedInstance : CommonVars = {
        let instance = CommonVars()
        return instance
    }()
    
    
    var personid : Int = 0
    var projectId : Int = 0
    var projectName = ""
    var contractId = 0
}

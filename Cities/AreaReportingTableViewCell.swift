//
//  AreaReportingTableViewCell.swift
//  Cities
//
//  Created by User on 18/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class AreaReportingTableViewCell: UITableViewCell {
    

    
    //MARK: - views
    @IBOutlet weak var igDropDown2: UIImageView!
    @IBOutlet weak var imgDropDown1: UIImageView!

    
    @IBOutlet weak var imgTxt2: UIImageView!
    @IBOutlet weak var imgTxt1: UIImageView!
    @IBOutlet weak var btnCheckBox1: CheckBox!
    @IBOutlet weak var btnCheckBox2: CheckBox!
    @IBOutlet weak var btnCheckBox3: CheckBox!
    @IBOutlet weak var btnCheckBox4: CheckBox!
    @IBOutlet weak var txtSumEmployees: UITextField!
    @IBOutlet weak var txtSumTools: UITextField!
    @IBOutlet weak var checkBox1Constraint: NSLayoutConstraint!
    @IBOutlet weak var tblDropDownConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblUnderTblHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTopToBtnDrop1Constraint: NSLayoutConstraint!
    @IBOutlet weak var tblDropBarries: UITableView!
    @IBOutlet weak var tblDropDownWorksInProcress: UITableView!
    @IBOutlet weak var tblDropDown: UITableView!
    @IBOutlet weak var btnDropDownWorksInProcress: UIButton!
    @IBOutlet weak var btnClickDropDown: UIButton!
    
    //MARK: - variables
    var flagIsEdit = false//דגל הבודק האם המשתמש עורך את הנתונים
    var personId : Int = 0
    var dropDownArr1 : Array<Any> = []
    var dropDownArr2 : Array<Any> = []
    var selectionsDropDownToEdit1  : Array<Any> = []
    var selectionsDropDownToEdit2  : Array<Any> = []
    var selectionsIndex1Arr : Array<Any> = []
    var count = 0
    var indexBtnDropDown = 0
    var indexButtonsArr = [Int]()
    var indexButtons = 0
    var b = UIButton()
    var b2 = UIButton()
    var b3 = UIButton()
    var showImageIndex = -1
    var flagWichDropDownClicked : Int?//1 - for the first drop, 2- for the second drop
    //    var tableActionDel : TableOptionDelegate?
    var arrProjectsName  : Array<Project> = Array<Project>()
    var barriersArr : Array<Table> = Array<Table>()
    var worksInProcessArr : Array<TablesToContract> = Array<TablesToContract>()
    var radioButtonController1 = SSRadioButtonsController()
    var selectionsComponentArr : Array<ContractReportComponents> = []//dropDown 1 - Component
    var selectionsHinderArr : Array<ContractReportHinders> = []//dropDown 2 - Hinder
    var tempComponentArr : Array<ContractReportComponents> = []
    var dropDown1IdArr : Array<Any> = []
    var dropDown2IdArr : Array<Any> = []
    var selectionsRowArr1 : Array<ContractReportComponents> = []
    var selectionsRowArr2 : Array<ContractReportHinders> = []
    var hideImageIndex  = -1
    var isRowSelected1 : Array<Bool> = []
    var isRowSelected2 : Array<Bool> = []
    var countRowSelected1 = 0
    var countRowSelected2 = 0
    var flagIsRowChecked = true
   
    //MARK: - actions
    @IBAction func checkBox1Action(_ sender: UIButton) {
        btnCheckBox1.isChecked = !btnCheckBox1.isChecked
    }
    @IBAction func checkBox2Action(_ sender: Any) {
        btnCheckBox2.isChecked = !btnCheckBox2.isChecked
    }
    @IBAction func checkBox3Action(_ sender: Any) {
        btnCheckBox3.isChecked = !btnCheckBox3.isChecked
    }
    @IBAction func checkBox4Action(_ sender: Any) {
        btnCheckBox4.isChecked = !btnCheckBox4.isChecked
    }
    
    
    @IBAction func btnDropDownWorksInProcressAction(_ sender: UIButton) {
        
        tblDropDown.isHidden = true
        
        
        flagWichDropDownClicked = 0
        b2 = sender
        b2.tag = 1
        if tblDropDownWorksInProcress.isHidden == true
        {tblDropDownWorksInProcress.isHidden = false
          
            
        }
            
        else
        {tblDropDownWorksInProcress.isHidden = true
//            isRowSelected = false
          btnDropDownWorksInProcress.setTitle("בחרת \(countRowSelected1) רשומות", for: .normal)
        }
    }
    
    @IBAction func btnClickDropAction(_ sender: UIButton) {
        //indexButtons = 0
        flagWichDropDownClicked = 1
        b3 = sender
        b3.tag = 0
        if sender.tag == 0
        {
            tblDropDownConstraint.constant = 0
            indexButtons = -1
        }
        
        if tblDropDown.isHidden == true
        {
            tblDropDown.isHidden = false
        }
            
        else
        {tblDropDown.isHidden = true
           
            btnClickDropDown.setTitle("בחרת \(countRowSelected2) רשומות", for: .normal)
        }
        
        
    }
    
    @IBAction func btnAddDropDown(_ sender: Any) {
        
        
        let btn = UIButton()
        b = btn
//        CommonVars.sharedInstance.countOfTbls += 1
        
        if let tbl = self.superview?.superview as? UITableView
        {
            tbl.beginUpdates()
            tbl.endUpdates()
        }
        
        btn.frame = CGRect(x: btnClickDropDown.frame.origin.x, y: btnClickDropDown.frame.origin.y + (btnClickDropDown.frame.height + 10) * CGFloat(count + 1), width: btnClickDropDown.frame.width, height: btnClickDropDown.frame.height)
        btn.backgroundColor = UIColor.red
        count += 1
        btn.tag = count
        lblUnderTblHeight.constant += 45
        
        self.contentView.addSubview(btn)
        self.contentView.sendSubview(toBack: btn)
        
        btn.addTarget(self, action: #selector(clickBtnDropDown(sender:indexRowPressed:)), for: .touchUpInside)
        
        //        tableActionDel?.setCount?(count: count)
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        getFieldsToEdit()
        //        hideKeyboardWhenTappedAround()
        imgTxt1.isHidden = true
        imgTxt2.isHidden = true
        self.tblDropBarries.allowsMultipleSelection = true
        txtSumTools.layer.cornerRadius = 7
        btnDropDownWorksInProcress.layer.cornerRadius = 7
        btnClickDropDown.layer.cornerRadius = 7
        tblDropBarries.layer.cornerRadius = 7
        tblDropDownWorksInProcress.layer.cornerRadius = 7
        self.contentView.bringSubview(toFront: tblDropDown)
        self.contentView.bringSubview(toFront: tblDropDownWorksInProcress)
        tblDropDownWorksInProcress.isHidden = true
        checkBox1Constraint.constant = 25.0
        lblUnderTblHeight.constant = 25.0
        lblTopToBtnDrop1Constraint.constant = 23.0
        tblDropBarries.isHidden = true
        getListComponents()//רכיבי ביצוע
        getListBarriers()//חסמים
        
        tblDropDownWorksInProcress.dataSource = self
        tblDropDownWorksInProcress.delegate = self
        tblDropBarries.delegate = self
        tblDropBarries.dataSource = self
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    //MARK: - functions
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReportsViewController.dismissKeyboard))
        self.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard()
    {
        self.endEditing(true)
        tblDropDownWorksInProcress.isHidden = true
    }
    
    func validationCheck() -> Bool
    {
        var flagIsFieldFull = false
        var countFiledsToReport = 0
        
        if txtSumTools.text?.isEmpty == true{
            txtSumTools.layer.borderColor = UIColor.red.cgColor
            imgTxt1.isHidden = false
        }
        else
        {
            imgTxt1.isHidden = true
            countFiledsToReport += 1
        }
        
        if txtSumEmployees.text?.isEmpty == true{
            txtSumEmployees.layer.borderColor = UIColor.red.cgColor
            imgTxt2.isHidden = true
        }
        else
        {
            imgTxt2.isHidden = true
            countFiledsToReport += 1
        }
        
        if countRowSelected1 == 0
        {
            imgDropDown1.image = UIImage(named: "group")
        }
        else
        {
            imgDropDown1.isHidden = true
            countFiledsToReport += 1
        }
        
        if countRowSelected2 == 0
        {
            igDropDown2.image = UIImage(named: "group")
        }
        else
        {
            igDropDown2.isHidden = true
              countFiledsToReport += 1
        }
        if countFiledsToReport == 4
        {
           flagIsFieldFull = true
        }
        else
        {
            flagIsFieldFull = false
        }
        return flagIsFieldFull
    }
    
    
    func clickBtnDropDown(sender : UIButton , indexRowPressed : Int)
    {
        //        UIApplication.shared.keyWindow?.bringSubview(toFront: tblDropBarries)
        //        tblDropBarries.bringSubview(toFront: sender)
        
        bringSubview(toFront: tblDropBarries)
        tblDropDownConstraint.constant = (btnClickDropDown.frame.height + 10) * CGFloat(sender.tag)
        indexButtons = sender.tag
        btnClickDropAction(sender)
        if indexRowPressed >= 0 && indexRowPressed < 10
        {sender.setTitle(dropDownArr2[indexRowPressed] as? String, for: .normal)}
        
    }
    
    
    //    func getListComponents()
    //    {
    //        let dic = Dictionary<String,AnyObject>()
    //        //            dic["iPersonId"] = CommonVars.sharedInstance.personid as AnyObject
    //
    //        api.sharedInstance.goServer(params: dic, success: {
    //            (param1, response) -> Void in
    //
    //            print("response : \(String(describing: response))")
    //
    //            if let result = response as? Array<Dictionary<String, AnyObject>> {
    //                let tbl = Table()
    //                //  project = project.getPersonFromDic(dic: result)
    //
    //                for item in result
    //                {
    //                    if let key = item["Key"] as? String, key == "Components" {
    //                        if let res1 = item["Value"] as? Array<Dictionary<String, AnyObject>> {
    //                            for item2 in res1 {
    //                                self.worksInProcessArr.append(tbl.getTableFromDic(dic: item2))
    //                            }
    //                        }
    //                    }
    //                    //                    self.barriersArr.append(tbl.getTableFromDic(dic: item))
    //                }
    //                //add to arr the name of the works in process
    //                for  i in self.worksInProcessArr
    //                {
    //                    self.dropDownArr1.append(i.nvName)
    //                }
    //                self.tblDropDownWorksInProcress.reloadData()
    //            }
    //
    //        }, failure: {
    //            (dataTask, error) -> Void in
    //
    //            print("error")
    //
    //        }, funcName: api.sharedInstance.GetCodeTables)
    //    }
    
    func getListComponents()
    {
        var dic = Dictionary<String,AnyObject>()
        
        dic["iContractId"] = CommonVars.sharedInstance.contractId as AnyObject
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            print("response : \(String(describing: response))")
            
            if let result = response as? Array<Dictionary<String, AnyObject>> {
                let t = TablesToContract()
                //                print(t.nvName)
                print(result)
                //  project = project.getPersonFromDic(dic: result)
                
                //
                for item in result
                {
                    if let key = item["Key"] as? String, key == "Components" {
                        if let res1 = item["Value"] as? Array<Dictionary<String, AnyObject>> {
                            for item2 in res1 {
                                self.worksInProcessArr.append(t.getTablesToContractFromDic(dic: item2))
                            }
                        }
                    }
                    //                    self.barriersArr.append(tbl.getTableFromDic(dic: item))
                }
                //add to arr the name of the barriers
                for  i in self.worksInProcessArr
                {
                    self.dropDownArr1.append(i.nvName)
                    //arr for save the id of componenets
                    self.dropDown1IdArr.append(i.iId)
                }
                self.tblDropDownWorksInProcress.reloadData()
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print("error")
            
        }, funcName: api.sharedInstance.GetCodeTablesToContract)
    }
    
    
    func getListBarriers()//חסמים
    {
        let dic = Dictionary<String,AnyObject>()
        //            dic["iPersonId"] = CommonVars.sharedInstance.personid as AnyObject
        
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            print("response : \(String(describing: response))")
            
            if let result = response as? Array<Dictionary<String, AnyObject>> {
                let tbl = Table()
                print(tbl.Components)
                //  project = project.getPersonFromDic(dic: result)
                
                for item in result
                {
                    if let key = item["Key"] as? String, key == "Hinders" {
                        if let res1 = item["Value"] as? Array<Dictionary<String, AnyObject>> {
                            for item2 in res1 {
                                self.barriersArr.append(tbl.getTableFromDic(dic: item2))
                            }
                        }
                    }
                }
                //add to arr the name of the barriers
                for  i in self.barriersArr
                {
                    self.dropDownArr2.append(i.nvName)
                    self.dropDown2IdArr.append(i.iId)
                }
                self.tblDropBarries.reloadData()
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print("error")
            
        }, funcName: api.sharedInstance.GetCodeTables)
    }
    

    func getFieldsToEdit()
    {
        var dic = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = 47 as AnyObject
        dic["iContractReportId"] = 231 as AnyObject
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            if let result = response as? Dictionary<String, AnyObject>{
                
                var get = GetContractReport()
                get = get.GetContractReportFromDic(dic: result)
                
                
                self.fiilTextFields(txt1: get.iNumToolsInArea, txt2: get.iNumWorkersInArea)
                self.fillDropDown1(arr: get.lContractReportComponents)
                self.fillDropDown2(arr: get.lContractReportHinders)
                self.tblDropDownWorksInProcress.reloadData()
                self.fillCheckBoxs(check1: get.bIsWorkContractSigned, check2: get.bIsExecutionEngineerExists, check3: get.bIsForemanExists, check4: get.bInSite)
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetContractReport)
    }
    
    
    func fiilTextFields(txt1 : Int, txt2 : Int)
    {
        txtSumTools.text = String(txt1)
        txtSumEmployees.text = String(txt2)
    }
    //fill up drop down with data that return from service
    func fillDropDown1(arr : Array<ContractReportComponents>)
    {
        for i in arr
        {
            selectionsDropDownToEdit1.append(i.iContractComponentType)
        }
        tblDropDownWorksInProcress.reloadData()
    }
    
    //fill down drop down
    func fillDropDown2(arr : Array<ContractReportHinders>)
    {
        for i in arr
        {
            selectionsDropDownToEdit2.append(i.iHinderType)
        }
    }
    
    
    func fillCheckBoxs(check1 : Bool, check2 : Bool, check3 : Bool, check4 : Bool)
    {
        btnCheckBox1.isChecked = check1
        btnCheckBox2.isChecked = check2
        btnCheckBox3.isChecked = check3
        btnCheckBox4.isChecked = check4
    }
   
}


//MARK: - extensions
extension AreaReportingTableViewCell: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath/*, a : Int*/) {
        if tableView == tblDropDownWorksInProcress//dropDown number 1 - רכיבי ביצוע
        {
            let c = ContractReportComponents()
            var indexSelected : Int = 0
            
            indexSelected = worksInProcessArr[indexPath.row].iId
            c.iContractComponentType = indexSelected
            selectionsRowArr1.append(c)
            
            if isRowSelected1[indexPath.row] == true
            {
                showImageIndex = indexPath.row
                isRowSelected1[indexPath.row] = false
                countRowSelected1 += 1
            }
            else if isRowSelected1[indexPath.row] == false
            {
                hideImageIndex = indexPath.row
                isRowSelected1[indexPath.row] = true
                countRowSelected1 -= 1
            }
//            tblDropDownWorksInProcress.reloadData()
            
        }
        
        if tableView == tblDropBarries//dropDown number 2  - חסמים
          {

            let c = ContractReportHinders()
            var indexSelected : Int = 0
            indexSelected = barriersArr[indexPath.row].iId
            c.iHinderType = indexSelected
            selectionsRowArr2.append(c)

            
            if isRowSelected2[indexPath.row] == true
            {
                showImageIndex = indexPath.row
                isRowSelected2[indexPath.row] = false
                countRowSelected2 += 1
            }
             else if isRowSelected2[indexPath.row] == false
            {
                hideImageIndex = indexPath.row
                isRowSelected2[indexPath.row] = true
                countRowSelected2 -= 1
            }
        }
        
       tableView.reloadData()
    }
    
//    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
//
//                if tableView == tblDropDownWorksInProcress
//                {
////                    let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableViewCell") as! DropDownTableViewCell
//
//                    hideImageIndex = indexPath.row
////                      cell.imgIndexDropDown.image = UIImage(named: "circle")
////                    tblDropDownWorksInProcress.reloadData()
//
//                }
//
//                if tableView == tblDropBarries
//                {
////                        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableViewCell") as! DropDownTableViewCell
////
////                    cell.imgIndexDropDown.image = UIImage(named: "circle")
////                    tblDropBarries.reloadData()
//
//                }
//
//
//    }
}


extension AreaReportingTableViewCell : UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownTableViewCell", for: indexPath) as! DropDownTableViewCell
        var index = 0
        cell.layer.borderWidth = 0.7
        
        if tableView == tblDropDownWorksInProcress
        {
            if flagIsEdit == true
            {
                if selectionsDropDownToEdit1[indexPath.row] as! Int == dropDown1IdArr[indexPath.row] as! Int
                {
                    cell.imgIndexDropDown.image = UIImage(named: "checkboxCircle")
                }
            }
            cell.lblDropDown.text = dropDownArr1[indexPath.row] as? String
            
            
            
//            if showImageIndex == indexPath.row
//            {
                if isRowSelected1[indexPath.row] == true
                {
                    cell.imgIndexDropDown.image = UIImage(named: "checkboxCircle")
                    showImageIndex = -1
                }
//            }
            
            
//            if hideImageIndex == indexPath.row
//            {
                if isRowSelected1[indexPath.row] == false
                {
                    cell.imgIndexDropDown.image = UIImage(named: "circle")
                    hideImageIndex = -1
                }
//            }
        }
        
        
        if tableView == tblDropBarries/*tblDropDown*/
        {
            
            cell.lblDropDown.text = dropDownArr2[indexPath.row] as? String
            if flagIsEdit == true
            {
                if selectionsDropDownToEdit2[indexPath.row] as! Int == dropDown2IdArr[indexPath.row] as! Int
                {
                    cell.imgIndexDropDown.image = UIImage(named: "checkboxCircle")
                }
            }
            
//            if showImageIndex == indexPath.row
//            {
            if isRowSelected2[indexPath.row] == true
            {
                cell.imgIndexDropDown.image = UIImage(named: "checkboxCircle")
                showImageIndex = -1
            }
            
            
//            if hideImageIndex == indexPath.row
//            {
            if isRowSelected2[indexPath.row] == false
            {
                cell.imgIndexDropDown.image = UIImage(named: "circle")
                hideImageIndex = -1
            }
            
        
        }
        
        
        if selectionsDropDownToEdit1.count > 0
        {
            let indexPath = IndexPath(row: selectionsDropDownToEdit1[index] as! Int, section: 0)
            tblDropDownWorksInProcress.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
            tblDropDownWorksInProcress.delegate?.tableView!(tblDropDownWorksInProcress, didDeselectRowAt: indexPath)
            index += 1
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == tblDropDownWorksInProcress
        {
            for i in dropDownArr1
            {
                isRowSelected1.append(false)
            }
            return dropDownArr1.count}
            
        else
        {
            for i in dropDownArr2
            {
                isRowSelected2.append(false)
            }
            return dropDownArr2.count}
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35.0
    }

    
}

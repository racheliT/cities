//
//  ReportsViewController.swift
//  Cities
//
//  Created by User on 18/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit
import CoreLocation


@objc protocol TableOptionDelegate {
    @objc optional func setCount(count : Int)
    @objc optional func uploadPhotos()
    @objc optional func getImage(image : UIImage)
}

class ReportsViewController: UIViewController, UINavigationControllerDelegate {
    
    //MARK: - views
    @IBOutlet weak var lblProjectName: UILabel!
    @IBOutlet weak var btnSendData: UIButton!
//    @IBOutlet weak var openOrCloseSection: UIButton!
    @IBOutlet weak var tblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tblReporting: UITableView!
    @IBOutlet weak var tblDropDownContract: UITableView!
    @IBOutlet weak var tblReportsHeight: NSLayoutConstraint!
  
    @IBOutlet weak var lblDetailsContract: UILabel!
    
    
    //MARK: - variabels
    var imageView = UIImageView()
    var picker:UIImagePickerController = UIImagePickerController()
    var isOpen : Array = [0,0,0]
    var sectionsNames = ["דיווח שטח","הערות וצירוף קבצים","דיווח שבועי"]
    var heightCell0 = 30
    var txtSelected = UITextField()
    let time = Data()
    let date = Date()
    var dayInWeek : Int = 5
    let formatter = DateFormatter()
    var DateResult : String?
    var cell1 = AreaReportingTableViewCell()
    var cell2 = CommentsTableViewCell()
    var cell3 = WeeklyReportingTableViewCell()
//    var cell = SectionReportTableViewCell()
    var check = CheckBox()
    var isSend : Bool = false//flag - if let to send the data' only all the fields are full
//    var projectId : Int = 0
    var selectionsToEdit  : Array<Any> = []
    var contractorName = ""
    var contractnum = 0
    var arr3 : Array<ContractReportFiles> = []
    var arr4 : Array<ContractReportFiles> = []
    var flagIsChangeImageArr : Array<Int> = [0,0,0]
    
    var manager = CLLocationManager()
    var contractReportWorkProgressFiles = ContractReportFiles()
    var contractReportNuisancesFiles = ContractReportFiles()
   
    var galleryDelegate : TableOptionDelegate?
    

    //MARK: - actions
    @IBAction func btnExitAction(_ sender: UIButton) {
        exit(0)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContractViewController") as! ContractViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
//        vc.getContractsByProject()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        lblProjectName.text = CommonVars.sharedInstance.projectName
//        lblDetailsContract.text = "\(contractorName) \(contractnum)"
        
        cell1 = tblReporting.dequeueReusableCell(withIdentifier: "AreaReportingTableViewCell") as! AreaReportingTableViewCell
        //        cell1.backgroundColor = UIColor.red
        cell2 = tblReporting.dequeueReusableCell(withIdentifier: "CommentsTableViewCell") as! CommentsTableViewCell
        
        cell3 = tblReporting.dequeueReusableCell(withIdentifier: "WeeklyReportingTableViewCell") as! WeeklyReportingTableViewCell
        cell2.galleryDelegate = self
        galleryDelegate = cell2
        //        self.btnSendData.bringSubview(toFront: btnSendData)
        //        tblReporting.isScrollEnabled = false
        hideKeyboardWhenTappedAround()
        
        tblReporting.delegate = self
        tblReporting.dataSource = self
        tblReportsHeight.constant = 0.0
        btnSendData.layer.cornerRadius = 7
        btnSendData.layer.borderWidth = 1
        //get the date of today
        formatter.dateFormat = "yyy.MM.dd"
        DateResult = formatter.string(from: date)
        
        btnSendData.addTarget(self, action: #selector(contractReports), for: .touchUpInside)
//         getFieldsToEdit()
        
        
        //        registerKeyboardNotifications()
        
        //        self.tblReporting.bringSubview(toFront: tblReporting)
        
        //createAlert(title: "דיווח שבועי", message: "יש לדווח")
        manager.delegate = self
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //MARK: - keyboard notifications
    //    func registerKeyboardNotifications() {
    //        NotificationCenter.default.addObserver(self,
    //                                               selector: #selector(self.keyboardDidShowTemp(notification:)),
    //                                               name: NSNotification.Name.UIKeyboardDidShow,
    //                                               object: nil)
    //
    //        NotificationCenter.default.addObserver(self,
    //                                               selector: #selector(self.keyboardWillHide(notification:)),
    //                                               name: NSNotification.Name.UIKeyboardDidHide,
    //                                               object: nil)
    //    }
    
    
    //MARK: - functions
//    func getDetailsToProject(projectid : Int)
//    {
//        projectId = projectid
//    }
    
    func getDetailsToContract(contName: String ,contNum: Int)
    {
//        contractId = contractid
        contractorName = contName
        contractnum = contNum
        lblDetailsContract.text = "\(contractorName) \(contractnum)"

    }
    
    func clickOnSection(sender : UIButton){

        //if the current day = 5 disable to open the section
        if sender.tag == 2 && dayInWeek == 5 && isOpen[sender.tag] == 0
        {isOpen[sender.tag] = 1
           
        }
        else if sender.tag == 2 && dayInWeek == 5 && isOpen[sender.tag] == 1
        {
            isOpen[sender.tag] = 0
        }
        
        if sender.tag == 2 && dayInWeek != 5
        {
            isOpen[sender.tag] = 0
        }

        if isOpen[sender.tag] == 1 && sender.tag != 2 //open
        {
            isOpen[sender.tag] = 0
        }
        else if isOpen[sender.tag] == 0 && sender.tag != 2
        {
            isOpen[sender.tag] = 1

        }
    
        tblReporting.reloadSections([sender.tag], with: .automatic)

        if isOpen[sender.tag] == 1 {
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
                self.tblReporting.scrollToRow(at: IndexPath(row: 0, section: sender.tag), at: .top, animated: true)
            })
        }
    }
    
    
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ReportsViewController.dismissKeyboard))
        tblReporting.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
        
    }
    
    func dismissKeyboard(){
        tblReporting.endEditing(true)
        //        cell1.tblDropDownWorksInProcress.isHidden = true
        //        cell1.tblDropBarries.isHidden = true
        //        cell3.tblDropDown1.isHidden = true
    }
    
    func keyboardDidShowTemp(notification : NSNotification) {
        var info = notification.userInfo
        let kbSize: CGSize? = (info?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        tblReporting.contentInset = UIEdgeInsetsMake(0, 0, (kbSize?.height)!, 0)
        tblReporting.scrollIndicatorInsets = tblReporting.contentInset
        //
        //        let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (kbSize?.height)!, 0.0)
        //        scrollView.contentInset = contentInsets!
        //        scrollView.scrollIndicatorInsets = contentInsets!
    }
    
    func getDayOfWeek(_ today:String) -> Int? {
        let formatter  = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        guard let todayDate = formatter.date(from: today) else { return nil }
        let myCalendar = Calendar(identifier: .gregorian)
//        dayInWeek = myCalendar.component(.weekday, from: todayDate)
        return dayInWeek
    }
    
    func createAlert(title : String, message : String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //    func keyboardWillHide(notification : NSNotification)
    //    {
    //        let contentInsets: UIEdgeInsets = UIEdgeInsets.zero
    //        tblReporting.contentInset = contentInsets
    //        tblReporting.scrollIndicatorInsets = contentInsets
    //    }
//
//    func addDataIntoHinderArr( arr : Array<ContractReportHinders>) -> Array<ContractReportHinders>
//    {
//        var arr = arr
//        for i in cell1.selectionsHinderArr
//        {
//            arr.append(i)
//        }
//        return arr
//    }
//
//    func addDataIntoProfessionalsArr( arr : Array<ContractReportProfessionals>) -> Array<ContractReportProfessionals>
//    {
//        var arr = arr
//        for i in cell3.selectionsProfessionalArr
//        {
//            arr.append(i)
//        }
//        return arr
//    }
//
    //    func addDataIntoFilesArr(var arr : Array<ContractReportFiles>) -> Array<ContractReportFiles>
    //    {
    //        for i in cell1.selections1Arr
    //        {
    //            if i.iHinderType >= 0
    //            {
    //                arr.append(i)
    //            }
    //        }
    //        return [arr]
    //    }
//
//    func addDataIntoComponentsArr( arr : Array<ContractReportComponents>) -> Array<ContractReportComponents>
//    {
//        var arr = arr
//        for i in cell1.selectionsRowArr1
//        {
//            arr.append(i)
//        }
//        return arr
//    }
//
    
    func validationChecks()//check if all the fields are full
    {
        var flag1 = false
        var flag3 = false
        let numCellsToReporting = 2//כמה סלים צריכים להיות מלאים לגמרי
        var countFullCells = 0//כמה סלים מלאים בפועל
        
        flagIsChangeImageArr[1] = 1
        //in the first cell - AreaReportTableViewCell
        flag1 = cell1.validationCheck()
//        flag2 = cell3.validationCheck()
        flag3 = cell3.validationCheck()
        
        if flag1 == true//cell1 is full
        {
             flagIsChangeImageArr[0] = 1//change iage to succes
            countFullCells += 1
        }
        else
        {   flagIsChangeImageArr[0] = 2}//change iage to error}

        
        if flag3 == true
        {
             flagIsChangeImageArr[2] = 1//change iage to succes
            countFullCells += 1
        }
        else
        {  flagIsChangeImageArr[2] = 2}//change iage to error}
        
        tblReporting.reloadData()
     
        //if all the field are full, can send the data to service
        if countFullCells == numCellsToReporting
        {
            isSend = true
        }
        else
        {
            createAlert(title: "", message: "לא הזנת נתונים בכל שדות הדיווח")
        }
    }
    
    func contractReports() {
        if isSend == true
        {
            Generic.sharedInstance.showNativeActivityIndicator(cont: self)
//            var dic = Dictionary<String, AnyObject>()
            var dict = Dictionary<String, AnyObject>()
            
//                        var arr1 : Array<ContractReportHinders> = []
//                        var arr2 : Array<ContractReportProfessionals> = []
//            var arr3 : Array<ContractReportFiles> = []
//            var arr4 : Array<ContractReportFiles> = []
//                        var arr5 : Array<ContractReportComponents> = []
            
            
            //        arr3 = addDataIntoFilesArr(arr: arr3)
            //        arr4 = addDataIntoFilesArr(arr: arr4)
            //            arr1 = addDataIntoHinderArr(arr: arr1)
            //            arr2 = addDataIntoProfessionalsArr(arr: arr2)
            //            arr5 = addDataIntoComponentsArr(arr: arr5)
            
            
//            let contractReportWorkProgressFiles = ContractReportFiles(_iContractReportId: -1, _nvFileName: "aa", _nvFilePath: "", _iFileType: 12, _nvLat: "", _nvLong: "")
            arr3.append(contractReportWorkProgressFiles)
            
            
//            let contractReportNuisancesFiles = ContractReportFiles(_iContractReportId: -1, _nvFileName: "rac", _nvFilePath: "", _iFileType: 13, _nvLat: "", _nvLong: "")
            arr4.append(contractReportNuisancesFiles)


            
            let contract = ContractReport(_iContractId: CommonVars.sharedInstance.contractId, _iProjectId: CommonVars.sharedInstance.projectId, _iPersonSupervisionId: CommonVars.sharedInstance.personid, _iNumToolsInArea: Int(cell1.txtSumTools.text!)!, _iNumWorkersInArea: Int(cell1.txtSumEmployees.text!)!, _bIsWorkContractSigned: cell1.btnCheckBox1.isChecked, _bIsExecutionEngineerExists: cell1.btnCheckBox2.isChecked, _bIsForemanExists: cell1.btnCheckBox3.isChecked, _bInSite: cell1.btnCheckBox4.isChecked, _nvComment: cell2.txtComment.text, _iWorkQualityRating: cell3.countStars, _iProgressType: cell3.segmentIndex, _lContractReportHinders: cell1.selectionsRowArr2, _lContractReportProfessionals: cell3.selectionsRowArr, _lContractReportWorkProgressFiles: arr3, _lContractReportNuisancesFiles: arr4, _lContractReportComponents: cell1.selectionsRowArr1)
            
            
            dict["contractReport"] = contract.getDicFromReport() as AnyObject
//         print(dict)
            api.sharedInstance.goServer(params: dict, success: {
                (param1, response) -> Void in
                
//                print("response : \(String(describing: response))")
                
                if let result = response as? Int {
                    
//                    print(result)
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SentDataViewController") as! SentDataViewController
                    Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }, failure: {
                (dataTask, error) -> Void in
                
                print(error.localizedDescription)
                
            }, funcName: api.sharedInstance.CreateNewContractReport)
        }
        else
        {
            validationChecks()

        }
    }
    
    
    
//    func getFieldsToEdit()
//    {
//        var dic = Dictionary<String,AnyObject>()
//        
//        dic["iUserId"] = 1 as AnyObject
//        dic["iContractReportId"] = 31 as AnyObject
//        api.sharedInstance.goServer(params: dic, success: {
//            (param1, response) -> Void in
//            
//            //            print("response : \(String(describing: response))")
//            
//            if let result = response as? Dictionary<String, AnyObject> {
//                var get = GetContractReport()
//                print("the result111111111 : ")
//                print(result)
//                get = get.GetContractReportFromDic(dic: result)
//                
//                self.fiilTextFields(txt1: get.iNumToolsInArea, txt2: get.iNumWorkersInArea)
//                self.fillDropDown(arr: get.lContractReportComponents)
//                self.cell1.tblDropDownWorksInProcress.reloadData()
//                self.fillCheckBoxs(check1: get.bIsWorkContractSigned, check2: get.bIsExecutionEngineerExists, check3: get.bIsForemanExists, check4: get.bInSite)
//            }
//            
//        }, failure: {
//            (dataTask, error) -> Void in
//            
//            print(error.localizedDescription)
//            
//        }, funcName: api.sharedInstance.GetContractReport)
//    }
    
    
    func fiilTextFields(txt1 : Int, txt2 : Int)
    {
        cell1.txtSumTools.text = String(txt1)
        cell1.txtSumEmployees.text = String(txt2)
    }
    
    func fillDropDown(arr : Array<ContractReportComponents>)
    {
        for i in arr
        {
            selectionsToEdit.append(i.iContractComponentType)
        }
    }
    
    func fillCheckBoxs(check1 : Bool, check2 : Bool, check3 : Bool, check4 : Bool)
    {
        cell1.btnCheckBox1.isChecked = check1
        cell1.btnCheckBox2.isChecked = check2
        cell1.btnCheckBox3.isChecked = check3
        cell1.btnCheckBox4.isChecked = check4
    }
    func getContractNameAndNum(name : String)
    {
//            lblDetailsContract.text = name
    }
    
    func openGallery()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker, animated: true, completion: nil)
        picker.delegate = self
    }
}



extension ReportsViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        
        switch indexPath.section {
        case 0:
            return cell1
        case 1:
            return cell2
        case 2:
            return cell3
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isOpen[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    
}


extension ReportsViewController : UITableViewDelegate{

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tblReporting.dequeueReusableCell(withIdentifier: "SectionReportTableViewCell") as! SectionReportTableViewCell

        if dayInWeek < 5
        {
            if section == 2
            {
                cell.contentView.backgroundColor = UIColor.gray
            }
        }
        if isOpen[section] == 1//open
        {
            cell.imgSection.image = UIImage(named: "minus")
            cell.openOrCloseSection.backgroundColor = UIColor.mrhSunYellow
        }
        else if isOpen[section] == 0
        {
            cell.imgSection.image = UIImage(named: "plus")
            cell.backgroundColor = UIColor.mrhFrenchBlueWithLowOpacity
        }
        if flagIsChangeImageArr[section] == 1
        {
            cell.imgSection.image = UIImage(named: "success")
//            flagIsChangeImageArr[section] = 0
        }
        else if flagIsChangeImageArr[section] == 2
        {
            cell.imgSection.image = UIImage(named: "error")
//            flagIsChangeImageArr[section] = 0
        }
   
        cell.lblTitleSection.text = sectionsNames[section]
        cell.openOrCloseSection.tag = section
        cell.openOrCloseSection.addTarget(self, action: #selector(clickOnSection), for:
            .touchUpInside)

        return cell.contentView
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
            
        case 0:
            return 660//CGFloat(580 + (100 * CommonVars.sharedInstance.countOfTbls))
        case 1:
            return 380
        case 2:
            return 380
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 80
    }
}
extension ReportsViewController :TableOptionDelegate
{
    func uploadPhotos() {
//        openGallery()
        showCameraActionSheet()
    }
    
//    func getImage(img : UIImage){
//
//
//    }
}


 extension ReportsViewController : UIImagePickerControllerDelegate
{
    
    func showCameraActionSheet()
    {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "צלם", style: .default, handler: {(alert: UIAlertAction!) in self.openCamera()})
        let libraryAction = UIAlertAction(title: "העלה תמונה", style: .default, handler: {(alert: UIAlertAction!) in self.openLibrary()})
        
        let cancelAction = UIAlertAction(title: "ביטול", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(libraryAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
//            Global.sharedInstance.bIsOpenFiles = true
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            //            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openLibrary()
    {
     
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
//            Global.sharedInstance.bIsOpenFiles = true
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.custom
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imageUrl = info[UIImagePickerControllerReferenceURL] as? NSURL{
            if let imageName = imageUrl.lastPathComponent{
                if let imageOriginal = info[UIImagePickerControllerOriginalImage] as? UIImage{
                    var image = imageOriginal
                    if picker.allowsEditing {
                        if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                            image = imageEdit
                        }
                    }
                    let imageString = image.toBase64(image: image)
                    if imageString != "" {
                       
                        galleryDelegate?.getImage!(image: image)
                        
                        
                        
                        
                        if cell2.isUpload1 == true
                        {
                            contractReportWorkProgressFiles.iContractReportId = -1
                            contractReportWorkProgressFiles.iFileType = 12
                            contractReportWorkProgressFiles.nvFileName = imageName
                            contractReportWorkProgressFiles.nvFilePath = imageString
                            
                        }
                        
                        if cell2.isUpload2 == true
                        {
                            contractReportNuisancesFiles.iContractReportId = -1
                            contractReportNuisancesFiles.iFileType = 13
                            contractReportNuisancesFiles.nvFileName = imageName
                            contractReportNuisancesFiles.nvFilePath = imageString
                        
                        }
                        
                        if manager.responds(to: #selector(manager.requestWhenInUseAuthorization)) {
                            manager.requestWhenInUseAuthorization()
                        }
                        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
                        manager.requestLocation()
//שמירת התמונה
//                        print("iameg base 64 : \(imageString)")
                        picker.dismiss(animated: true, completion: nil)
                    } else {
//                        Alert.sharedInstance.showAlert(mess: "not success")
                        picker.dismiss(animated: true, completion: nil)
                    }
                }
            }
        } else {
            if let imageOriginal = info[UIImagePickerControllerOriginalImage]as? UIImage{
                var image = imageOriginal
                if picker.allowsEditing {
                    if let imageEdit = info[UIImagePickerControllerEditedImage] as? UIImage {
                        image = imageEdit
                    }
                }
                
                let imageName = "img.JPEG"
                let imageString = image.toBase64(image: image)
                if imageString != "" {
                    galleryDelegate?.getImage!(image: image)
                    
                    if cell2.isUpload2 == true
                    {
                        
                        contractReportNuisancesFiles.iContractReportId = -1
                        contractReportNuisancesFiles.iFileType = 13
                        contractReportNuisancesFiles.nvFileName = imageName
                        contractReportNuisancesFiles.nvFilePath = imageString
                    }
                    
                    if cell2.isUpload1 == true
                    {
                        contractReportWorkProgressFiles.iContractReportId = -1
                        contractReportWorkProgressFiles.iFileType = 12
                        contractReportWorkProgressFiles.nvFileName = imageName
                        contractReportWorkProgressFiles.nvFilePath = imageString
                    }
                    
                    if manager.responds(to: #selector(manager.requestWhenInUseAuthorization)) {
                        manager.requestWhenInUseAuthorization()
                       
                    }
                    
                    manager.requestLocation()
                    Generic.sharedInstance.showNativeActivityIndicator(cont: self)
                    picker.dismiss(animated: true, completion: nil)
                    
                }else{
//                    Alert.sharedInstance.showAlert(mess: NSLocalizedString(LocalizableStringStatic.sharedInstance.BAD_IMAGE, comment: ""))
                    picker.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}

extension ReportsViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print("Found user's location: \(location)")
            
            if cell2.isUpload1
            {
                contractReportWorkProgressFiles.nvLat = String(location.coordinate.latitude)
                contractReportWorkProgressFiles.nvLong = String(location.coordinate.longitude)
             cell2.isUpload1 = false
            } else if cell2.isUpload2
            {
            contractReportNuisancesFiles.nvLat = String(location.coordinate.latitude)
            contractReportNuisancesFiles.nvLong = String(location.coordinate.longitude)
                cell2.isUpload2 = false
            }

        }
        Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
}



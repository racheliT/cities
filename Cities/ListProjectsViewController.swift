//
//  ListProjectsViewController.swift
//  Cities
//
//  Created by User on 30/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ListProjectsViewController: UIViewController {

    //MARK: - views
    @IBOutlet weak var tblProjects: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblHeightConstraint: NSLayoutConstraint!
    
    //MARK: - variables
    var tempArr1 : Array<Project> = Array<Project>()
    var projectNameArr : Array<Any> = []
    var tempArr2 : Array<Contract> = []
    var contractNumberArr :Array<Any> = []
    var contractorNameArr : Array<String> = []
    var projectIdArr : Array<Any> = []

    
    //MARK: - actions
    @IBAction func btnBackAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LogInViewController") as! LogInViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        self.navigationController?.pushViewController(vc, animated: true)
        
      btnBack.setImage(UIImage.init(named : "back"), for: .normal)
        getListProjects()
        
        //lblHeightConstraint.constant = CGFloat(projectNameArr.count * 40)
        tblProjects.delegate = self
        tblProjects.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - serviceFunctions
    func getListProjects()
    {
            var dic = Dictionary<String,AnyObject>()
            dic["iPersonId"] = CommonVars.sharedInstance.personid as AnyObject
    
            api.sharedInstance.goServer(params: dic, success: {
                (param1, response) -> Void in
    
//                print("response : \(String(describing: response))")
    
    
                if let result = response as? Array<Dictionary<String, AnyObject>> {
                    let project = Project()
    
                    //  project = project.getPersonFromDic(dic: result)
    
                    for item in result
                    {
                        self.tempArr1.append(project.getPersonFromDic(dic: item))
                    }
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ReportsViewController") as! ReportsViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
                    for a in self.tempArr1
                    {
                        self.projectNameArr.append(a.nvProjectName)
//                        vc.getDetailsToProject(projectid: a.iProjectId)
                        self.projectIdArr.append(a.iProjectId)
                        
                    }
                    self.tblProjects.reloadData()
                }
    
            }, failure: {
                (dataTask, error) -> Void in
    
//                print(error.localizedDescription)
                
            }, funcName: api.sharedInstance.GetProjectsToSupervision)
          
        }
    
    
    
//    
//    func getContractsByProject(_projectid : Int)
//    {
//        var dic = Dictionary<String, AnyObject>()
//        
//        dic["iProjectId"] = _projectid as AnyObject
//        //        let person = Person(userName: "ווביט פיתוח", passWord: "147852",personType: 1)
//        //        dic["person"] = person.getDicFromPerson() as AnyObject
//        //        dic["date"] = khjkh
//        //        print(dic)
//        api.sharedInstance.goServer(params: dic, success: {
//            (param1, response) -> Void in
//
////            print(response!)
//            if let result = response as? Array<Dictionary<String, AnyObject>> {
////                print(result)
//                let c = Contract()
//                
//                for item in result
//                {
//                    self.tempArr2.append(c.getContractFromDic(dic: item))
//                    
//                }
//                for item in self.tempArr2
//                {
//                    self.contractNumberArr.append(item.nvContractNumber)
//                    self.contractorNameArr.append(item.nvContractorName)
//                    print(item.nvContractNumber)
//                    print(item.nvContractorName)
//                    
//                }
////                c = c.getContractFromDic(dic: result)
////                print(c.nvContractNumber)
//                
//                //                self.vc.getPersonId(personid: person.iPersonId)
//            }
//            
//        }, failure: {
//            (dataTask, error) -> Void in
//            
//            print(error.localizedDescription)
//            
//        }, funcName: api.sharedInstance.GetContractsToProject)
//    }

}

extension ListProjectsViewController : UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblProjects.dequeueReusableCell(withIdentifier: "ListProjectsTableViewCell") as! ListProjectsTableViewCell
        
        cell.backgroundColor = UIColor.mrhFrenchBlueWithLowOpacity
        
        
//        cell.imgArrow.image = UIImage(named: "img")
        CommonVars.sharedInstance.projectName = projectNameArr[indexPath.row] as! String
        cell.lblProjectName.text = projectNameArr[indexPath.row] as? String
//        cell.layer.borderWidth = 1
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContractViewController") as! ContractViewController
//        var projectid = 0
        
//        projectid = projectIdArr[indexPath.row] as! Int
        //        getContractsByProject(_projectid: projectid)
//        vc.getDetailsOfProject(projectid: projectIdArr[indexPath.row] as! Int/*projectid, projectname: projectNameArr[indexPath.row] as! String*/)//send id of project to to the contract list
        CommonVars.sharedInstance.projectId = projectIdArr[indexPath.row] as! Int
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
}

extension ListProjectsViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return projectNameArr.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

//
//  GetContractReport.swift
//  Cities
//
//  Created by User on 06/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class GetContractReport: NSObject {
    //מחזירה את פרטי הדוח לעריכה
    
    var bInSite : Bool = true
    var bIsExecutionEngineerExists : Bool = true
    var bIsForemanExists : Bool = true
    var bIsWorkContractSigned : Bool = true
    var dtCreateDate : String = ""//"/Date(1509967903790+0200)/",
    var iContractId : Int = 0
    var iContractReportId : Int = 0// 31,
    var iNumToolsInArea :Int =  5
    var iNumWorkersInArea : Int =  0//23
    var iPersonSupervisionId : Int = 0// 2
    var iProgressType : Int = 0// 554,
    var iProjectId : Int = 0// 11,
    var iWorkQualityRating : Int = 0// 3,
    var lContractReportComponents : Array<ContractReportComponents> = Array<ContractReportComponents>()
    var lContractReportHinders : Array<ContractReportHinders> = Array<ContractReportHinders>()
    var lContractReportNuisancesFiles : Array<ContractReportFiles> = Array<ContractReportFiles> ()
    var lContractReportProfessionals : Array<ContractReportProfessionals> = Array<ContractReportProfessionals>()
    var lContractReportWorkProgressFiles : Array<ContractReportFiles> = Array<ContractReportFiles>()
    var nvComment : String = ""
    var nvContractNumber : String = ""
    var nvContractReportComponents : String = ""
    var nvContractReportHinders : String = ""
    var nvContractReportNuisancesFiles : String = ""
    var nvContractReportProfessionals : String = ""
    var nvContractReportWorkProgressFiles : String = ""
    var nvContractorName : String = ""
    var nvPersonSupervision : String = ""
    var nvProgressType :String = ""
    var nvProjectName : String = ""
    var nvProjectNumber : String = ""
    
    
//    func getDicFromGetContractReport() -> Dictionary<String, AnyObject> {
//        var dic = Dictionary<String, AnyObject>()
//        
    
//        dic["iProjectId"] = iProjectId as AnyObject
//        
//        
//        return dic
//        
//    }
    
    
    
    func GetContractReportFromDic(dic : Dictionary<String, AnyObject>) -> GetContractReport {
        
        let g = GetContractReport()
        
        if dic["bInSite"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["bInSite"]!)  {
                g.bInSite = item as! Bool
                
            }
        }
        
        
        if dic["bIsExecutionEngineerExists"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["bIsExecutionEngineerExists"]!)  {
                g.bIsExecutionEngineerExists = item as! Bool
            }
        }
        
        
        if dic["bIsForemanExists"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["bIsForemanExists"]!)  {
                g.bIsForemanExists = item as! Bool
                
            }
        }
        
        
        if dic["bIsWorkContractSigned"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["bIsWorkContractSigned"]!)  {
                g.bIsWorkContractSigned = item as! Bool
                
            }
        }
        
        if dic["iContractId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iContractId"]!) as? NSNumber {
                g.iContractId = item as! Int
                
            }
        }
        
        if dic["iContractReportId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iContractReportId"]!) as? NSNumber {
                g.iContractReportId = item as! Int
                
            }
        }
        
        if dic["iNumToolsInArea"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iNumToolsInArea"]!) as? NSNumber {
                g.iNumToolsInArea = item as! Int
                
            }
        }
        //
        if dic["iNumWorkersInArea"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iNumWorkersInArea"]!) as? NSNumber {
                g.iNumWorkersInArea = item as! Int
                
            }
        }
        
        if dic["iPersonSupervisionId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iPersonSupervisionId"]!) as? NSNumber {
                g.iPersonSupervisionId = item as! Int
                
            }
        }
        
        if dic["iProgressType"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iProgressType"]!) as? NSNumber {
                g.iProgressType = item as! Int
                
            }
        }
        //
        if dic["iProjectId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iProjectId"]!) as? NSNumber {
                g.iProjectId = item as! Int
                
            }
        }
        
        if dic["iWorkQualityRating"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iWorkQualityRating"]!) as? NSNumber {
                g.iWorkQualityRating = item as! Int
                
            }
        }
        //array
        if dic["lContractReportComponents"] != nil {
            var arr : Array<ContractReportComponents> = []
             let c = ContractReportComponents()
            for i in dic["lContractReportComponents"] as! Array<AnyObject>
            {
                arr.append(c.getContractReportComponentsFromDic(dic: i as! Dictionary<String, AnyObject>))
                
            }
            
            g.lContractReportComponents = arr
        }
        
        if dic["lContractReportHinders"] != nil {
            var arr : Array<ContractReportHinders> = []
            for i in dic["lContractReportHinders"] as! Array<AnyObject>
            {
                arr.append(ContractReportHinders().getContractReportHindersFromDic(dic: i as! Dictionary<String, AnyObject>))
                
            }
            
        }
        
        if dic["lContractReportHinders"] != nil {
            var arr : Array<ContractReportHinders> = []
            for i in dic["lContractReportHinders"] as! Array<AnyObject>
            {
                arr.append(ContractReportHinders().getContractReportHindersFromDic(dic: i as! Dictionary<String, AnyObject>))
                
            }
            g.lContractReportHinders = arr
        }
        
        return g
    }
}

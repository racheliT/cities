//
//  Contract.swift
//  Cities
//
//  Created by User on 05/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class Contract: NSObject {

    var iContractId : Int = 0
    
    var iProjectId : Int = 0
    
    var nvContractNumber : String = ""
    
    var nvContractorName : String = ""
//    
//    public DateTime? dtStartContractDate { get; set; }
//    
//    public DateTime? dtEndContractDate { get; set; }
//    `
//    public int iContractStatusType { get; set; }
    
//    var lContractComponents Array<ContractReportComponents> = Array<ContractReportComponents>()
//    
//    public string nvContractComponents { get; set; }
//    
//    public List<ContractProfessionals> lContractProfessionals { get; set; }
//    
//    public string nvContractProfessionals { get; set; }
    
    override init() {
        
    }
    init(_iProjectId : Int)
    {
        iProjectId = _iProjectId
    }
    
    
    
    func getDicFromContract() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iProjectId"] = iProjectId as AnyObject
        
        
        return dic
        
    }
    
    
    
    func getContractFromDic(dic : Dictionary<String, AnyObject>) -> Contract {
        
        let c = Contract()
        
        if dic["nvContractNumber"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvContractNumber"]!)  {
                c.nvContractNumber = item as! String
                
            }
        }
        
        
        if dic["nvContractorName"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvContractorName"]!)  {
                c.nvContractorName = item as! String
                
            }
        }
        
        if dic["iContractId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iContractId"]!)  {
                c.iContractId = item as! Int
                
            }
        }
         return c
    }
}

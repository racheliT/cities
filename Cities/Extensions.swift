//
//  Extensions.swift
//  Cities
//
//  Created by User on 12/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

//class Extensions: NSObject {

    extension UIImage{
        func toBase64(image : UIImage) -> String
        {
            let imageData = UIImagePNGRepresentation(self)
            return (imageData?.base64EncodedString(options: .lineLength64Characters))!
//            var imageData:String/*NSString*/
//            var dataForJPEGFile:NSData
//
//            dataForJPEGFile = UIImageJPEGRepresentation(image, 1)! as NSData
//            //        dataForJPEGFile = UIImagePNGRepresentation(image)! as NSData
//            //        dataForJPEGFile = UIImageJPEGRepresentation(image, 1)! as NSData
//            let imageSize: Int = dataForJPEGFile.length
//            //        print("size of image in KB: %f ", imageSize/1024.0)
//            if imageSize <= 4000000{//ולא 4 מליון כיוון שמוצא את משקל התמונה אחרי המרה שהוא הרבה יותר כבד
//                imageData = dataForJPEGFile.base64EncodedString(options: [])
//
//                return imageData/* as String*/
//            }
////            else{
////                let newImage = resizeImage(image: image, newWidth: image.size.width/2)
////                dataForJPEGFile = UIImageJPEGRepresentation(newImage, 1)! as NSData
////                let imageSize: Int = dataForJPEGFile.length
////                if imageSize <= 4000000{
////                    imageData = dataForJPEGFile.base64EncodedString(options: [])
////
////                    return imageData/* as String*/
////                }
//                return ""
            }
        }
            
        



    extension UIColor {
        
        @nonobjc class var mrhWhite: UIColor {
            return UIColor(white: 255.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhSunYellow: UIColor {
            return UIColor(red: 254.0 / 255.0, green: 215.0 / 255.0, blue: 51.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhCobalt74: UIColor {
            return UIColor(red: 32.0 / 255.0, green: 55.0 / 255.0, blue: 134.0 / 255.0, alpha: 0.74)
        }
        
        @nonobjc class var mrhBlack: UIColor {
            return UIColor(white: 0.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhSquash50: UIColor {
            return UIColor(red: 240.0 / 255.0, green: 198.0 / 255.0, blue: 21.0 / 255.0, alpha: 0.5)
        }
        
        @nonobjc class var mrhWarmGrey: UIColor {
            return UIColor(white: 155.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhFrenchBlue: UIColor {
            return UIColor(red: 58.0 / 255.0, green: 80.0 / 255.0, blue: 167.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhFrenchBlueWithLowOpacity: UIColor {
            return UIColor(red: 58.0 / 255.0, green: 80.0 / 255.0, blue: 167.0 / 255.0, alpha: 0.65)
        }
        
        @nonobjc class var mrhWhiteTwo: UIColor {
            return UIColor(white: 216.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhBlackTwo: UIColor {
            return UIColor(white: 3.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhBrightBlue: UIColor {
            return UIColor(red: 0.0, green: 122.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhDenimBlue: UIColor {
            return UIColor(red: 58.0 / 255.0, green: 80.0 / 255.0, blue: 157.0 / 255.0, alpha: 1.0)
        }
        
        @nonobjc class var mrhPurpleyGrey: UIColor {
            return UIColor(red: 138.0 / 255.0, green: 136.0 / 255.0, blue: 139.0 / 255.0, alpha: 1.0)
        }
}
//}


//
//  Project.swift
//  Cities
//
//  Created by User on 26/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class Project: NSObject {

    var iProjectId : Int = 0
    var iPersonSupervisionId : Int = 0
    var nvSupervisionName : String = ""
    var iProjectStatusType : Int = 0
    var nvProjectStatusType : String = ""
    var nvProjectName : String = ""
    var nvLongProjectName : String = ""
    var nvProjectNumber : String = ""
    var iCityProjectType : Int = 0
    var nvCityProjectType : String = ""
    
    override init() {
        
    }
    
//    init(fn : String, ln : String) {
//        nvFirstName = fn
//        nvLastName = ln
//    }
//    
//    init(_iProjectId : Int) {
//        iProjectId = _iProjectId
//        
//    }
    
    
    func getDicFromPerson() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["iProjectId"] = iProjectId as AnyObject
        dic["iPersonSupervisionId"] = iPersonSupervisionId as AnyObject
        
        return dic
        
    }
    
    
    
    func getPersonFromDic(dic : Dictionary<String, AnyObject>) -> Project {
        
        let project = Project()
        
        if dic["iProjectId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iProjectId"]!) as? NSNumber {
                project.iProjectId = item as! Int
               
            }
        }
        
        if dic["iPersonSupervisionId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iPersonSupervisionId"]!) as? NSNumber {
                project.iPersonSupervisionId = item as! Int
                
            }
        }
        
        if dic["nvProjectName"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvProjectName"]!)  {
                project.nvProjectName = item as! String
                
            }
        }
        
      
//
//        if dic["iPersonSupervisionId"] != nil {
//            
//            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iPersonSupervisionId"]!) as? NSNumber {
//                
//                project.iPersonSupervisionId = Int(item)
//            }
//        }
//        
//        if dic["nvSupervisionName"] != nil {
//            
//            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvSupervisionName"]!) {
//                
//                project.nvSupervisionName = item as! String
//            }
//        }

        return project
        
    }
  
    
}

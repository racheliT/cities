//
//  ContractReportProfessionals.swift
//  Cities
//
//  Created by User on 01/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ContractReportProfessionals: NSObject {

    var iContractReportId : Int = 0
    var iContractProfessionalType : Int = 0

    override init(){}
    init(/*_iContractReportId : Int, */_iContractProfessionalType : Int)
    {
//        iContractReportId = _iContractReportId
        iContractProfessionalType = _iContractProfessionalType
    }
    
    func getDicFromContractReportProfessionals() -> Dictionary<String, AnyObject>
    {
        var dic = Dictionary<String, AnyObject>()
        
//        dic["iContractReportId"] = iContractReportId as AnyObject
        dic["iContractProfessionalType"] = iContractProfessionalType as AnyObject
        return dic
    }
}

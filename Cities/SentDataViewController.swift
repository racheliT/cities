//
//  SentDataViewController.swift
//  Cities
//
//  Created by User on 12/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class SentDataViewController: UIViewController {
    
 
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - actions
    @IBAction func btnBackAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContractViewController") as! ContractViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnExitAction(_ sender: Any) {
        exit(0)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       lblTitle.text = CommonVars.sharedInstance.projectName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}

//
//  Person.swift
//  Cities
//
//  Created by User on 26/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class Person: NSObject {
    
    var nvUserName : String = ""
    var nvPassword : String = ""
    var iPersonType : Int = 0
    var iPersonId : Int = 0
    var nvPersonType : String = ""
    var nvFirstName : String = ""
    var nvLastName : String = ""
    var nvName : String = ""
    var nvIdentity : String = ""
    var nvMobile : String = ""
    var nvEmail : String = ""
    var iPersonStatusType : Int = 0
    var lProjects : String = ""
    
    
//    var age : Float = 0.0
//    var homeNumber : Int = 0
    
    override init() {
        
    }
    
    init(userName : String, passWord : String, personType : Int) {
        nvUserName = userName
        nvPassword = passWord
        iPersonType = personType
    }
//    sending
    func getDicFromPerson() -> Dictionary<String, AnyObject> {
        var dic = Dictionary<String, AnyObject>()
        
        dic["nvUserName"] = nvUserName as AnyObject
        dic["nvPassword"] = nvPassword as AnyObject
        
        return dic
        
    }

    func getPersonFromDic(dic : Dictionary<String, AnyObject>) -> Person {
        
        let person = Person()
 
        
        if dic["nvUserName"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvUserName"]!) {
               person.nvUserName = item as! String
            }
        }
        
        if dic["nvPassword"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvPassword"]!) {
                person.nvPassword = item as! String
            }
        }
        
        if dic["iPersonId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iPersonId"]!) as? NSNumber {
                person.iPersonId = item as! Int
            }
        }
        
//        if dic["age"] != nil {
//            
//            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["age"]!) as? NSNumber {
//                
//                person.age = Float(item)
//            }
//        }
//        
//        if dic["homeNumber"] != nil {
//            
//            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["homeNumber"]!) as? NSNumber {
//                
//                person.homeNumber = Int(item)
//            }
//        }
//
//        
        
        return person
        
    }

}

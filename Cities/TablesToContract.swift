//
//  TablesToContract.swift
//  Cities
//
//  Created by User on 06/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class TablesToContract: NSObject {
    
//מביא את רכיבי הביצוע ורשימת פיקוח עליון
    var iId : Int = 0
    var nvName : String = ""
    
    
    
//    func getDicFromTablesToContract() -> Dictionary<String, AnyObject> {
//        var dic = Dictionary<String, AnyObject>()
//        
//        dic["iProjectId"] = iProjectId as AnyObject
//        dic["iPersonSupervisionId"] = iPersonSupervisionId as AnyObject
//        
//        return dic
//        
//    }
    
    
    
    func getTablesToContractFromDic(dic : Dictionary<String, AnyObject>) -> TablesToContract {
        
        let t = TablesToContract()
        
        if dic["iId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iId"]!) as? NSNumber {
                t.iId = item as! Int
                
            }
        }
        
        if dic["nvName"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvName"]!){
                t.nvName = item as! String
                
            }
        }
        return t
    }
    
    
    
}






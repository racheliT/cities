//
//  Barrier.swift
//  Cities
//
//  Created by User on 30/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class Table: NSObject {
    
    var iId : Int = 0
    var nvName : String = ""
    var Components : String = ""
    var Hinders : String = ""
    
//    override init(){
//    }
    
//    init(id : Int, name : String, componet : String, hinder : String){
//        iId = id
//        nvName = name
//        Components = componet
//        Hinders = hinder
//    }
    
    
    func getTableFromDic(dic : Dictionary<String, AnyObject>) -> Table {
        
        let tbl1 = Table()
        
        if dic["iId"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iId"]!)
                as? NSNumber {
                tbl1.iId = item as! Int
            }
        }
        if dic["nvName"] != nil {
            
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["nvName"]!)  {
                tbl1.nvName = item as! String
            }
        }
//        
//        if dic["Components"] != nil {
//            
//            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["Components"]!)  {
//                tbl1.Components = item as! String
//                
//            }
//        }
//        
//        if dic["Hinders"] != nil {
//            
//            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["Hinders"]!)  {
//                tbl1.Hinders = item as! String
//                
//            }
//        }
        
        return tbl1
    
    }
}

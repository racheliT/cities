//
//  KeyboardTryViewController.swift
//  Cities
//
//  Created by User on 18/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class KeyboardTryViewController: UIViewController {

    
    //MARK: - Views
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var txtUp: UITextField!
    @IBOutlet weak var txtDown: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        hideKeyboardWhenTappedAround()
        
        txtUp.delegate = self
        txtDown.delegate = self
        registerKeyboardNotifications()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: - keyBoard notifications
    var txtSelected = UITextField()
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardDidShowTemp(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    var scrollHeight : CGFloat = 0
    
//    func keyboardDidShow(notification: NSNotification)
//    {
//        //        if self.scrollView.contentOffset.y <= 0 {
//        //Need to calculate keyboard exact size due to Apple suggestions
//        scrollHeight = scrollView.contentSize.height
//        self.scrollView.isScrollEnabled = true
//        let info : NSDictionary = notification.userInfo! as NSDictionary
//        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
//        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize!.height, 0.0)
//        
//        self.scrollView.contentInset = contentInsets
//        self.scrollView.scrollIndicatorInsets = contentInsets
//        
//        var aRect : CGRect = self.view.frame
//        aRect.size.height -= keyboardSize!.height
//        
//        //        if txtSelected.tag == 1 {  // text field on a view
//        
//        var bRect : CGRect = txtSelected.frame
//        
//        // to do : slash to check
//        if txtSelected.tag == 1 {
//            bRect.origin.y += (txtSelected.superview?.frame.origin.y)!
//        } else if txtSelected.tag == 2 {
//            bRect.origin.y += ((txtSelected.superview?.frame.origin.y)! + 130)
//        }
//        
//        if (!aRect.contains(bRect)) {
//            bRect.origin.y = bRect.origin.y - aRect.height  + bRect.height
//            bRect.origin.x = 0
//            self.scrollView.setContentOffset(bRect.origin, animated: true)
//        }
//        
//        if txtSelected.tag == 2 {
//            bRect.origin.x = 0
//            self.scrollView.setContentOffset(bRect.origin, animated: true)
//        }
//        //        }
//    }
    
    func keyboardDidShowTemp(notification : NSNotification) {
        var aRect: CGRect = view.frame
//        if txtSelected.tag != 2 {
            var info: [AnyHashable: Any]? = notification.userInfo
            let kbSize: CGSize? = (info?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
         //   let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (kbSize?.height)!, 0.0)
//            scrollView.contentInset = contentInsets!
//            scrollView.scrollIndicatorInsets = contentInsets!
        
            // If active text field is hidden by keyboard, scroll it so it's visible
            // Your application might not need or want this behavior.
            
            aRect.size.height -= (kbSize?.height)!
//        }
        //brect
        var bRect: CGRect = txtSelected.frame
        
        if txtSelected.tag == 1/* || txtSelected.tag == 2*/ {
            bRect.origin.y += (txtSelected.superview?.frame.origin.y)!
        }
        
//        if txtSelected.tag == 2 {
//            bRect.origin.y += 100
//            let scrollPoint = CGPoint(x: CGFloat(0.0), y: bRect.origin.y)
//            scrollView.setContentOffset(scrollPoint, animated: true)
//        }
//        else
            if !aRect.contains(bRect.origin) {
                let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y))
                //            let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y - (kbSize?.height)!))
                scrollView.setContentOffset(scrollPoint, animated: true)
        }
    }
    
    func keyboardWillHide(notification: NSNotification)
    {
//        //Once keyboard disappears;, restore original positions
//                let info : NSDictionary = notification.userInfo! as NSDictionary
//                let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
//                let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
//        
//               self.scrollView.contentInset = contentInsets
//        
//        
//                self.scrollView.scrollIndicatorInsets = contentInsets
//                changeScrollViewHeight(heightToadd: -((keyboardSize?.height)!))
//        
//                self.view.endEditing(true)
//        
//               scrollView.contentSize.height = scrollHeight
//              self.scrollView.isScrollEnabled = false
//        
//        
//        
//        
//       // var contentInsets: UIEdgeInsets = UIEdgeInsets.zero
//        scrollView.contentInset = contentInsets
//        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
//    func getTextField(textField: UITextField, originY: CGFloat) {
//        self.txtSelected = textField
//    }
    
    
  }

extension KeyboardTryViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSelected = textField
        
        return true
    }
    
    

}

//
//  CommonFunc.swift
//  Cities
//
//  Created by User on 26/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class CommonFunc: NSObject {
    override init() {
        
    }
    
    static let sharedInstance : CommonFunc = {
        let instance = CommonFunc()
        return instance
    }()
    
    func parseJsonToAnyObject(anyToParse:AnyObject)-> AnyObject?
    {
        if anyToParse is NSNumber {
            return anyToParse
        }

        if anyToParse is String
        {
            let checkIfParse = String((anyToParse.description)!)
            if checkIfParse == "<null>" {
                return nil
            } else {
                return checkIfParse as AnyObject
            }
        }
        return nil
    }

    

  
}

//
//  LogInViewController.swift
//  Cities
//
//  Created by User on 17/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {
    
    //MARK: - views
    @IBOutlet weak var imgTxtPW: UIImageView!
    @IBOutlet weak var imgTxtUserName: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPassWord: UILabel!
    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnEnter: UIButton!
    @IBOutlet weak var lblLoginNotRight: UILabel!
    
    @IBOutlet weak var txtPWConstraint: NSLayoutConstraint!
    //MARK: - variables
    var keyboardHeight : Int = 0
    var flagLogin  = false
//    var vc = AreaReportingTableViewCell()
    //    var arr  : Array<Project> = Array<Project>()
     var scrollHeight : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        imgTxtUserName.isHidden = true
        imgTxtPW.isHidden = true
        txtUserName.layer.borderColor = UIColor.mrhPurpleyGrey.cgColor
        txtUserName.layer.cornerRadius = 5
        txtUserName.layer.borderWidth = 1
        txtPassword.layer.cornerRadius = 5
        txtPassword.layer.borderWidth = 1
        txtPassword.layer.borderColor = UIColor.mrhPurpleyGrey.cgColor
        //txtUserName.keyboardType = .asciiCapable
        btnEnter.layer.cornerRadius = 8
        btnEnter.layer.borderWidth = 1
        
        //  btnEnter.layer.borderColor = UIColor.red1.cgColor
        
        //MARK: - connect To Extensions
        //        txtUserName.delegate = self as? UITextFieldDelegate
        //        txtPassword.delegate = self as? UITextFieldDelegate
        //        registerKeyboardNotifications()
        hideKeyboardWhenTappedAround()
        txtPassword.delegate = self
        txtUserName.delegate = self
        
//        vc = self.storyboard?.instantiateViewController(withIdentifier: "AreaReportingTableViewCell") as! AreaReportingTableViewCell
        //        vc.getFieldText(txt: txtName.text!)
        //        self.navigationController?.pushViewController(vc, animated: true)
        

        
        //MARK: - define notifications
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        
//
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - actions
    @IBAction func btnClickToEnterAction(_ sender: Any) {
        
        
        validationCheck()
        login()
//        if flagLogin == true
//        {
//            lblLoginNotRight.text = ""
//            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListProjectsViewController") as! ListProjectsViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }

    //MARK: - keyBoard notifications
    var txtSelected = UITextField()
//    func registerKeyboardNotifications() {
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.keyboardDidShowTemp(notification:)),
//                                               name: NSNotification.Name.UIKeyboardDidShow,
//                                               object: nil)
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.keyboardWillHide(notification:)),
//                                               name: NSNotification.Name.UIKeyboardWillHide,
//                                               object: nil)
//    }

//    func keyboardDidShowTemp(notification : NSNotification) {
//        var aRect: CGRect = view.frame
//        //        if txtSelected.tag != 2 {
//        var info: [AnyHashable: Any]? = notification.userInfo
//        let kbSize: CGSize? = (info?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
//           let contentInsets: UIEdgeInsets? = UIEdgeInsetsMake(0.0, 0.0, (kbSize?.height)!, 0.0)
//                    scrollView.contentInset = contentInsets!
//                    scrollView.scrollIndicatorInsets = contentInsets!
//        
//     
//        
//        aRect.size.height -= (kbSize?.height)!
//        //        }
//        //brect
//        var bRect: CGRect = txtSelected.frame
//        
//        if txtSelected.tag == 1/* || txtSelected.tag == 2*/ {
//            bRect.origin.y += (txtSelected.superview?.frame.origin.y)!
//        }
//        
//        //        if txtSelected.tag == 2 {
//        //            bRect.origin.y += 100
//        //            let scrollPoint = CGPoint(x: CGFloat(0.0), y: bRect.origin.y)
//        //            scrollView.setContentOffset(scrollPoint, animated: true)
//        //        }
//        //        else
//        if !aRect.contains(bRect.origin) {
//            let scrollPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y))
//                        _ = CGPoint(x: CGFloat(0.0), y: CGFloat(bRect.origin.y - (kbSize?.height)!))
//            scrollView.setContentOffset(scrollPoint, animated: true)
//        }
//    }
//
//    func keyboardWillHide(notification: NSNotification)
//    {
//        //        //Once keyboard disappears;, restore original positions
//                        let info : NSDictionary = notification.userInfo! as NSDictionary
//                        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
//                        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize!.height, 0.0)
//        
//                       self.scrollView.contentInset = contentInsets
//        
//        
//                        self.scrollView.scrollIndicatorInsets = contentInsets
////                        var changeScrollViewHeight(heightToadd: -((keyboardSize?.height)!))
//        
//                        self.view.endEditing(true)
//        
//                       scrollView.contentSize.height = scrollHeight
//                      self.scrollView.isScrollEnabled = false
   
//               // var contentInsets: UIEdgeInsets = UIEdgeInsets.zero
//                scrollView.contentInset = contentInsets
//                scrollView.scrollIndicatorInsets = contentInsets
    //    }
    
    //MARK: - functions
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func validationCheck()
    {
        if txtUserName.text?.isEmpty == true
        {
            txtUserName.layer.borderColor = UIColor.red.cgColor
            lblLoginNotRight.text = ""
//             imgTxtUserName.image = #imageLiteral(resourceName: "group.png")
            imgTxtUserName.isHidden = false
        }
            
        else
        {txtUserName.layer.borderColor = UIColor.black.cgColor
           imgTxtUserName.isHidden = true
        }
        
        if txtPassword.text?.isEmpty == true
        {
            txtPassword.layer.borderColor = UIColor.red.cgColor
            lblLoginNotRight.text = ""
//             imgTxtPW.image = #imageLiteral(resourceName: "group.png")
            imgTxtPW.isHidden = false
        }
        else
        {txtPassword.layer.borderColor = UIColor.black.cgColor
             imgTxtPW.isHidden = true
        }
    }
    
    
    
    func login() {
        var dic = Dictionary<String, AnyObject>()
        
        dic["nvUserName"] = txtUserName.text as AnyObject
        dic["nvPassword"] = txtPassword.text as AnyObject
        dic["iPersonType"] = 2 as AnyObject
        
        //        let person = Person(userName: "ווביט פיתוח", passWord: "147852",personType: 1)
        //        dic["person"] = person.getDicFromPerson() as AnyObject
        //        dic["date"] = khjkh
        //        print(dic)
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            if response == nil
            {
                if self.txtUserName.text?.isEmpty == false && self.txtPassword.text?.isEmpty == false
                {
                    self.lblLoginNotRight.text = "שם משתמש או סיסמא שגויים"
                    self.lblLoginNotRight.textColor = UIColor.red
                }
//                self.flagLogin = false
            }
//            else
//            {self.flagLogin = true}
            
//            print("response : \(String(describing: response))")
            
            if let result = response as? Dictionary<String, AnyObject> {
                
                var person = Person()
                
                person = person.getPersonFromDic(dic: result)
                CommonVars.sharedInstance.personid = person.iPersonId
                
                self.lblLoginNotRight.text = ""
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ListProjectsViewController") as! ListProjectsViewController
                self.navigationController?.pushViewController(vc, animated: true)
                //                self.vc.getPersonId(personid: person.iPersonId)
//                print("\(person.nvUserName) \(person.nvPassword)")
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.LoginUser)
        
    }
    
    //    func getTracks() {
    //        Generic.sharedInstance.showNativeActivityIndicator(cont: self)
    //        var dic = Dictionary<String, AnyObject>()
//        dic["nvDeviceId"] = CommonFunc.sharedInstance.getDeviceId() as AnyObject
//        dic["nvGuide"] = UserDefaultManagment.sharedInstance.getGuide() as AnyObject
//        let fromDays : TimeInterval = (24 * 60 * 60) * -7
//        dic["dtFrom"] = CommonFunc.sharedInstance.convertDateToSendServer(dateToConvert: CommonFunc.sharedInstance.isRoleTypeAsBouncer() ? Date() : Date(timeIntervalSinceNow: fromDays)) as AnyObject
//        let toDays : TimeInterval = (24 * 60 * 60) * 14
//        dic["dtTo"] = CommonFunc.sharedInstance.convertDateToSendServer(dateToConvert: CommonFunc.sharedInstance.isRoleTypeAsBouncer() ? Date() : Date(timeIntervalSinceNow: toDays)) as AnyObject
//        dic["iPersonId"] = CommonVars.sharedInstance.person.iPersonId as AnyObject
//        dic["iCustomerRoleTypeId"] = CommonVars.sharedInstance.person.iCustomerRoleTypeId/*.rawValue*/ as AnyObject
//        dic["iCustomerId"] = CommonVars.sharedInstance.person.iCustomerId as AnyObject
//        print(dic as Any)
//        api.sharedInstance.goServer(params : dic,success : {
//            (operation:AFHTTPRequestOperation?, responseObject:Any?) -> Void in
//            
//            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
//            print(responseObject as! [String:AnyObject])
//            
//            if let dicResult = (responseObject as? Dictionary<String, AnyObject>) {
//                
//                var keyValue = KeyValue<Int, Array<Track>>()
//                keyValue = keyValue.getKeyValueFromDic(dic: dicResult)
//                
//                if keyValue.value != nil {
//                    
//                    if keyValue.key == -1 {
//                        Alert.sharedInstance.showAlert(vc: self.navigationController, mess: "שגיאה!")
//                    } else if keyValue.key == 1 {
//                        self.getUpdatedTracks(tracksList: keyValue.value!)
//                    }
//                } else {
//                    Alert.sharedInstance.showAlert(vc: self.navigationController, mess: "שגיאה!")
//                }
//                
//            }
//        } ,failure : {
//            (AFHTTPRequestOperation, Error) -> Void in
//            Generic.sharedInstance.hideNativeActivityIndicator(cont: self)
//            Alert.sharedInstance.showAlert(vc: self.navigationController, mess: NSLocalizedString("אין חיבור לאינטרנט!", comment: ""))
//        }, funcName : api.sharedInstance.GetTracksByIdAndRoleType)
//    }
//
//    

    //MARK: - functions
//    func keyboardWillShow(notification : NSNotification)
//    {
//        
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            keyboardHeight = Int(keyboardSize.height)
//            print("(keyboardHeight 1:) \(keyboardHeight)")
//        }
//        
//        
//        if let info = notification.userInfo {
//            let rect : CGRect = info["UIKeyboardFrameEndUserInfoKey"] as! CGRect
//            
//        
//            
//            self.view.layoutIfNeeded()
//            print("(hight 1:) \(self.txtPWConstraint.constant)")
//            UIView.animate(withDuration: 0.25, animations: {
//                self.view.layoutIfNeeded()
//                
//                self.txtPWConstraint.constant = CGFloat(Int(self.txtPWConstraint.constant) - self.keyboardHeight) * -1
//                //self.textFielfBottomConstraint.constant += CGFloat(self.keyboardHeight)
//                print("(hight 2:) \(self.txtPWConstraint.constant)")
//                //rect.height - 200
//            })
//        }
//    }

    
}



extension LogInViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtSelected = textField
        txtSelected.resignFirstResponder()
        return true
    }
    
    
    
}


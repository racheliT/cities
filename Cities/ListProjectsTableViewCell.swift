//
//  ListProjectsTableViewCell.swift
//  Cities
//
//  Created by User on 30/10/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ListProjectsTableViewCell: UITableViewCell {

    //MARK: - views
    @IBOutlet weak var lblProjectName: UILabel!
    
    @IBOutlet weak var imgArrow: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

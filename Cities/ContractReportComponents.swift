//
//  ContractReportComponents.swift
//  Cities
//
//  Created by User on 01/11/2017.
//  Copyright © 2017 User. All rights reserved.
//

import UIKit

class ContractReportComponents: NSObject {

    var iContractReportId : Int = 0
    var iContractComponentType : Int = 0

    override init(){}
    
    init(/*_iContractReportId : Int, */_iContractComponentType : Int){
//        iContractReportId = _iContractReportId
        iContractComponentType = _iContractComponentType
    }
    
    func getContractReportComponentsFromDic(dic : Dictionary<String, AnyObject>) -> ContractReportComponents
    {
        var c = ContractReportComponents()
        
        if dic["iContractComponentType"] != nil
        {
            if let item = CommonFunc.sharedInstance.parseJsonToAnyObject(anyToParse: dic["iContractComponentType"]!) as? NSNumber {
                c.iContractComponentType = item as! Int}
        }
            return c
    }
    
        
        
     func getDicFromContractReportComponents() -> Dictionary<String, AnyObject>
     {
        var dic = Dictionary<String, AnyObject>()
        
//        dic["iContractReportId"] = iContractReportId as AnyObject
        dic["iContractComponentType"] = iContractComponentType as AnyObject
        return dic
    }
    
    
}

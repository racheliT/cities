//
//  api.swift
//  FineWork
//
//  Created by Racheli Kroiz on 4.12.2016.
//  Copyright © 2016 webit. All rights reserved.
//

import UIKit
import AFNetworking


class api: NSObject {

    let manager = AFHTTPSessionManager()
    
    typealias successBlock =  (URLSessionDataTask, Any?) -> Void
    
    typealias failureBlock = (URLSessionDataTask?, Error) -> Void

    static let sharedInstance : api = {
        let instance = api()
        return instance
        }()
    
    override init()
    {
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        
        manager.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
//        manager.requestSerializer.stringEncoding = String.Encoding.utf8.rawValue
        
        manager.responseSerializer.acceptableContentTypes = ["application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav"]
        
//        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as! Set<NSObject>
    }
    
//    func buldUrlFile(fileName:String)->String{
//        var pathUrl = url + "Files/" + fileName
//        pathUrl = pathUrl.replacingOccurrences(of: "\\", with: "/")
//        return pathUrl
//    }
//
    
    var urlQA = "http://qa.webit-track.com/ArimQAWS/Service1.svc/";
    var urlLive = "?";
    
    enum WS {
        case Live
        case QA
    }
    
    var WSType : WS = .QA
    
    func getUrlBase() -> String {
        
        switch WSType {
        case .Live:
            return urlLive
        case .QA:
            return urlQA
        }
    }
    
    func post(_ URLString: String, parameters: Any, success: ((_ operation: /*AFHTTPRequestOperation*/AFHTTPRequestSerializer, _ responseObject: AnyObject) -> Void)! , failure: ((_ operation: AFHTTPRequestSerializer /*AFHTTPRequestOperation*/, _ error: Error) -> Void)!) -> /*AFHTTPRequestOperation*/AFHTTPRequestSerializer {
        
        return /*AFHTTPRequestOperation*/AFHTTPRequestSerializer()
        
    }
    
    //MARK: - General
    func goServer(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!, funcName : String) {
        //let path = "GetWorker"
        manager.post(self.getUrlBase() + funcName, parameters: params, progress: nil, success: success, failure: failure)
//        manager.post(self.getUrlBase() + funcName, parameters: params, success: success,failure: failure)
    }
    
    func getDataFromGoogleApi(url: String, success: (successBlock)!, failure: (failureBlock)!) {
        manager.get(url, parameters: nil, success: success, failure: failure)
    }
    
    //Google api url
    func makeGoogleURL(sourceLat : Double, sourceLng: Double, destLat: Double, destLng: Double) -> String {
        
        /// free key
        let url = "http://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLat),\(sourceLng)&destination=\(destLat),\(destLng)&sensor=true&units=metric&mode=driving"
        /*+ "&key=" + context.getString(R.string.abcde)*/;
        
        // pay key
        //        String url = "https://maps.googleapis.com/maps/api/directions/json?"
        //
        //                + "origin=" + sourceLat + "," + sourceLng
        //                + "&destination=" + destLat + "," + destLng
        //                + "&sensor=true&units=metric&mode=driving"
        //                + "&key=" + context.getString(R.string.abcde);
        //
        
        return url;
    }
    
    let webitLogsUrl = "http://qa.webit-track.com/WebitLogs/LogService.svc/WriteLog"
    func sendLogToWebitLogs(params:Dictionary<String,AnyObject>, success: (successBlock)!, failure: (failureBlock)!) {
        //let path = "GetWorker"
        manager.post(self.webitLogsUrl, parameters: params, success: success,failure: failure)
    }


    
  //MARK: - Functions Names
    var LoginUser = "LoginUser"
    var GetProjectsToSupervision = "GetProjectsToSupervision"
    var GetCodeTables = "GetCodeTables"
    var CreateNewContractReport = "CreateNewContractReport"
    var GetContractsToProject = "GetContractsToProject"
    var GetCodeTablesToContract = "GetCodeTablesToContract"
    var GetContractReport = "GetContractReport"
}

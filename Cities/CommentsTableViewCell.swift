//
//  CommentsTableViewCell.swift
//  Cities
//
//  Created by User on 18/10/2017.
//  Copyright © 2017 User. All rights reserved.
//


import UIKit

class CommentsTableViewCell: UITableViewCell{
    
    
    //MARK: - views
    @IBOutlet weak var btnImg1: UIButton!
    
    @IBOutlet weak var btnImg2: UIButton!
    @IBOutlet weak var btnUpload1: UIButton!
    @IBOutlet weak var btnUpload2: UIButton!
    @IBOutlet weak var txtComment: UITextView!
    // @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - variables
    var isUpload1 = false
    var isUpload2 = false
    var galleryDelegate : TableOptionDelegate?
    
    var imageButtonTemp : UIButton?
    var countFillImages = 0
    //MARK: - actions
    
    @IBAction func imgClickAction1(_ sender: UIButton) {
        
        imageButtonTemp = sender
        
        
    }
    
    @IBAction func btnUpload2Action(_ sender: Any) {
        isUpload2 = true
        galleryDelegate?.uploadPhotos?()
//         isUpload1 = false
    }
    
    @IBAction func btnUpload1Action(_ sender: Any) {
        isUpload1 = true
         galleryDelegate?.uploadPhotos?()
//         isUpload1 = false
    }
  
  
   
    override func awakeFromNib() {
        super.awakeFromNib()
        getFieldsToEdit()
        
        // Initialization code
        //        let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
        //        alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler: nil))
        //        //self.displa(alert, animated: true, completion: nil)
        //        //alert.show(CommentsTableViewCell, sender: nil)
        //        alert.show(CommentsTableViewCell as UITableViewCell, sender: <#Any?#>)
        //
        //        if !MFMailComposeViewController.canSendMail() {
        //            print("Mail services are not available")
        //            return
        //        }
        btnUpload1.setImage(UIImage(named: "attachment"), for: .normal)
        btnUpload2.setImage(UIImage(named: "attachment"), for: .normal)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - functions

    
    func getFieldsToEdit()
    {
        //        “iUserId”:1,“iContractReportId”:1
        var dic = Dictionary<String,AnyObject>()
        
        dic["iUserId"] = 1 as AnyObject
        dic["iContractReportId"] = 31 as AnyObject
        api.sharedInstance.goServer(params: dic, success: {
            (param1, response) -> Void in
            
            //            print("response : \(String(describing: response))")
            
            if let result = response as? Dictionary<String, AnyObject> {
                var get = GetContractReport()
                get = get.GetContractReportFromDic(dic: result)
               self.fillComment(txt: get.nvComment)
            }
            
        }, failure: {
            (dataTask, error) -> Void in
            
            print(error.localizedDescription)
            
        }, funcName: api.sharedInstance.GetContractReport)
    }

    func fillComment(txt : String)
    {
        txtComment.text = txt
    }
}


extension CommentsTableViewCell : TableOptionDelegate {
    func getImage(image: UIImage) {
        countFillImages += 1
//        imageButtonTemp?.setImage(image, for: .normal)
        switch countFillImages {
        case 1:
            btnImg1.setImage(image, for: .normal)
        case 2:
            btnImg2.setImage(image, for: .normal)
        default:
           break
        }
    }
}
    

